package ru.renessans.jvschool.volkov.task.manager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.TaskEndpoint;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@SuppressWarnings("unused")
public final class TaskViewByTitleCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_TASK_VIEW_BY_TITLE = "task-view-by-title";

    @NotNull
    private static final String DESC_TASK_VIEW_BY_TITLE = "просмотреть задачу по заголовку";

    @NotNull
    private static final String NOTIFY_TASK_VIEW_BY_TITLE =
            "Происходит попытка инициализации отображения задачи. \n" +
                    "Для отображения задачи по заголовку введите заголовок задачи из списка. ";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_TASK_VIEW_BY_TITLE;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_TASK_VIEW_BY_TITLE;
    }

    @Override
    public void execute() {
        @NotNull final IServiceLocatorService serviceLocator = super.locatorService.getServiceLocator();
        @NotNull final ICurrentSessionService currentSessionService = serviceLocator.getCurrentSession();
        @Nullable final SessionDTO current = currentSessionService.getSession();

        ViewUtil.print(NOTIFY_TASK_VIEW_BY_TITLE);
        @NotNull final String title = ViewUtil.getLine();

        @NotNull final IEndpointLocatorService endpointLocator = super.locatorService.getEndpointLocator();
        @NotNull final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        @Nullable final TaskDTO task = taskEndpoint.getTaskByTitle(current, title);
        ViewUtil.print(task);
    }

}