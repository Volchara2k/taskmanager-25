package ru.renessans.jvschool.volkov.task.manager.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.ILocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IEndpointLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IServiceLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICommandService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.exception.unknown.UnknownCommandException;
import ru.renessans.jvschool.volkov.task.manager.repository.EndpointLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.repository.ServiceLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.service.EndpointLocatorService;
import ru.renessans.jvschool.volkov.task.manager.service.LocatorService;
import ru.renessans.jvschool.volkov.task.manager.service.ServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.util.ScannerUtil;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import java.util.Objects;

public final class Bootstrap {

    @NotNull
    private final IServiceLocatorRepository serviceLocatorRepository = new ServiceLocatorRepository();

    @NotNull
    private final IServiceLocatorService serviceLocator = new ServiceLocatorService(serviceLocatorRepository);

    @NotNull
    private final IEndpointLocatorRepository endpointLocatorRepository = new EndpointLocatorRepository();

    @NotNull
    private final IEndpointLocatorService endpointLocator = new EndpointLocatorService(endpointLocatorRepository);

    @NotNull
    private final ICommandService commandService = serviceLocator.getCommandService();

    @NotNull
    private final ILocatorService locatorService = new LocatorService(
            endpointLocator, serviceLocator
    );

    {
        this.commandService.createCommands(this.locatorService);
    }

    public void run(@Nullable final String... arguments) {
        final boolean isEmptyArgs = ValidRuleUtil.isNullOrEmpty(arguments);
        if (isEmptyArgs) terminalCommandExecuteLoop();
        else argumentExecute(Objects.requireNonNull(arguments[0]));
    }

    @SuppressWarnings("InfiniteLoopStatement")
    private void terminalCommandExecuteLoop() {
        @NotNull String commandLine;
        while (true) {
            try {
                commandLine = ScannerUtil.getLine();
                @Nullable final AbstractCommand command = this.commandService.getTerminalCommand(commandLine);
                if (Objects.isNull(command)) throw new UnknownCommandException(commandLine);
                command.execute();
            } catch (@NotNull final Exception exception) {
                System.err.print(exception.getMessage() + "\n");
            }
        }
    }

    private void argumentExecute(@NotNull final String argument) {
        try {
            @Nullable final AbstractCommand command = this.commandService.getArgumentCommand(argument);
            if (Objects.isNull(command)) throw new UnknownCommandException(argument);
            command.execute();
        } catch (@NotNull final Exception exception) {
            System.err.print(exception.getMessage() + "\n");
        }
    }

}