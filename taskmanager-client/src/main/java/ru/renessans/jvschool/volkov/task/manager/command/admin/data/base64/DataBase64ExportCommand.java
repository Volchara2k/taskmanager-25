package ru.renessans.jvschool.volkov.task.manager.command.admin.data.base64;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.DomainDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@SuppressWarnings("unused")
public final class DataBase64ExportCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_BASE64_EXPORT = "data-base64-export";

    @NotNull
    private static final String DESC_BASE64_EXPORT = "экспортировать домен в base64 вид";

    @NotNull
    private static final String NOTIFY_BASE64_EXPORT = "Происходит процесс выгрузки домена в base64 вид...";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_BASE64_EXPORT;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_BASE64_EXPORT;
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final IServiceLocatorService serviceLocator = super.locatorService.getServiceLocator();
        @NotNull final ICurrentSessionService currentSessionService = serviceLocator.getCurrentSession();
        @Nullable final SessionDTO current = currentSessionService.getSession();

        @NotNull final IEndpointLocatorService endpointLocator = super.locatorService.getEndpointLocator();
        @NotNull final AdminDataInterChangeEndpoint dataEndpoint = endpointLocator.getAdminDataInterChangeEndpoint();

        @NotNull final DomainDTO domain = dataEndpoint.exportDataBase64(current);
        ViewUtil.print(NOTIFY_BASE64_EXPORT);
        ViewUtil.print(domain.getUsers());
        ViewUtil.print(domain.getTasks());
        ViewUtil.print(domain.getProjects());
    }

}
