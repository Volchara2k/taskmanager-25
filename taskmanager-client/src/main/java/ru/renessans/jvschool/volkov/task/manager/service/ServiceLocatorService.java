package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IServiceLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICommandService;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceLocatorService;

@AllArgsConstructor
public final class ServiceLocatorService implements IServiceLocatorService {

    @NotNull
    private final IServiceLocatorRepository serviceLocatorRepository;

    @NotNull
    @Override
    public ICurrentSessionService getCurrentSession() {
        return this.serviceLocatorRepository.getCurrentSession();
    }

    @NotNull
    @Override
    public ICommandService getCommandService() {
        return this.serviceLocatorRepository.getCommandService();
    }

}