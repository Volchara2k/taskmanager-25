package ru.renessans.jvschool.volkov.task.manager.repository;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IServiceLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ICommandRepository;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ICurrentSessionRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICommandService;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.service.CommandService;
import ru.renessans.jvschool.volkov.task.manager.service.CurrentSessionService;

public final class ServiceLocatorRepository implements IServiceLocatorRepository {

    @NotNull
    private final ICurrentSessionRepository sessionRepository = new CurrentSessionRepository();

    @NotNull
    private final ICurrentSessionService sessionService = new CurrentSessionService(
            sessionRepository
    );

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    @Override
    public ICurrentSessionService getCurrentSession() {
        return this.sessionService;
    }

    @NotNull
    @Override
    public ICommandService getCommandService() {
        return this.commandService;
    }

}