package ru.renessans.jvschool.volkov.task.manager.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.TaskEndpoint;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@SuppressWarnings("unused")
public final class TaskCreateCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_TASK_CREATE = "task-create";

    @NotNull
    private static final String DESC_TASK_CREATE = "добавить новую задачу";

    @NotNull
    private static final String NOTIFY_TASK_CREATE =
            "Происходит попытка инициализации создания задачи. \n" +
                    "Для создания задачи введите заголовок проекта для задачи, заголовок задачи и описание. ";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_TASK_CREATE;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_TASK_CREATE;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final IServiceLocatorService serviceLocator = super.locatorService.getServiceLocator();
        @NotNull final ICurrentSessionService currentSessionService = serviceLocator.getCurrentSession();
        @Nullable final SessionDTO current = currentSessionService.getSession();

        ViewUtil.print(NOTIFY_TASK_CREATE);
        @NotNull final String projectTitle = ViewUtil.getLine();
        @NotNull final String title = ViewUtil.getLine();
        @NotNull final String description = ViewUtil.getLine();

        @NotNull final IEndpointLocatorService endpointLocator = super.locatorService.getEndpointLocator();
        @NotNull final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        @Nullable final TaskDTO task = taskEndpoint.addTaskForProject(current, projectTitle, title, description);
        ViewUtil.print(task);
    }

}