package ru.renessans.jvschool.volkov.task.manager.command.system;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICommandService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

import java.util.Collection;

@SuppressWarnings("unused")
public final class HelpListCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_HELP = "help";

    @NotNull
    private static final String ARG_HELP = "-h";

    @NotNull
    private static final String DESC_HELP = "вывод списка команд";

    @NotNull
    private static final String NOTIFY_HELP = "Список команд";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_HELP;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARG_HELP;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_HELP;
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final IServiceLocatorService serviceLocator = super.locatorService.getServiceLocator();
        @NotNull final ICommandService commandService = serviceLocator.getCommandService();
        @Nullable final Collection<AbstractCommand> commands = commandService.getAllCommands();
        ViewUtil.print(NOTIFY_HELP);
        ViewUtil.print(commands);
    }

}