package ru.renessans.jvschool.volkov.task.manager.exception.illegal;

import org.jetbrains.annotations.NotNull;

public final class IllegalInstantiationException extends RuntimeException {

    @NotNull
    private static final String ILLEGAL_INSTANTIATION =
            "Ошибка! Параметр \"объект класса команда\" является нелегальным и не может быть создан!\n";

    public IllegalInstantiationException() {
        super(ILLEGAL_INSTANTIATION);
    }

    public IllegalInstantiationException(@NotNull final Throwable cause) {
        super(cause);
    }

}