package ru.renessans.jvschool.volkov.task.manager.command.admin.data.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@SuppressWarnings("unused")
public final class DataXmlClearCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_XML_CLEAR = "data-xml-clear";

    @NotNull
    private static final String DESC_XML_CLEAR = "очистить xml данные";

    @NotNull
    private static final String NOTIFY_XML_CLEAR = "Происходит процесс очищения xml данных...";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_XML_CLEAR;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_XML_CLEAR;
    }

    @Override
    public void execute() throws Exception {
        @NotNull final IServiceLocatorService serviceLocator = super.locatorService.getServiceLocator();
        @NotNull final ICurrentSessionService currentSessionService = serviceLocator.getCurrentSession();
        @Nullable final SessionDTO current = currentSessionService.getSession();

        @NotNull final IEndpointLocatorService endpointLocator = super.locatorService.getEndpointLocator();
        @NotNull final AdminDataInterChangeEndpoint dataEndpoint = endpointLocator.getAdminDataInterChangeEndpoint();

        final boolean removeState = dataEndpoint.dataXmlClear(current);
        ViewUtil.print(NOTIFY_XML_CLEAR);
        ViewUtil.print(removeState);
    }

}