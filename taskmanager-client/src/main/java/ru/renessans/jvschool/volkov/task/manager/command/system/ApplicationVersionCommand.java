package ru.renessans.jvschool.volkov.task.manager.command.system;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@SuppressWarnings("unused")
public final class ApplicationVersionCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_VERSION = "version";

    @NotNull
    private static final String ARG_VERSION = "-v";

    @NotNull
    private static final String DESC_VERSION = "вывод версии программы";

    @NotNull
    private static final String NOTIFY_VERSION = "Версия: 1.0.25.";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_VERSION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARG_VERSION;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_VERSION;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_VERSION);
    }

}