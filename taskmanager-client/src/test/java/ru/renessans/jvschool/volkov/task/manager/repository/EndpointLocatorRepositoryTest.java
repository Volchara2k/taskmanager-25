package ru.renessans.jvschool.volkov.task.manager.repository;

import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IEndpointLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.endpoint.*;
import ru.renessans.jvschool.volkov.task.manager.marker.IntegrationImplementation;

@Category(IntegrationImplementation.class)
public final class EndpointLocatorRepositoryTest {

    @NotNull
    private final IEndpointLocatorRepository endpointLocatorRepository = new EndpointLocatorRepository();

    @Test
    @TestCaseName("Run testGetAuthenticationEndpoint for getAuthenticationEndpoint()")
    public void testGetAuthenticationEndpoint() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        @NotNull final AuthenticationEndpoint endpoint = this.endpointLocatorRepository.getAuthenticationEndpoint();
        Assert.assertNotNull(endpoint);
    }

    @Test
    @TestCaseName("Run testGetAdminEndpoint for getAdminEndpoint()")
    public void testGetAdminEndpoint() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        @NotNull final AdminEndpoint endpoint = this.endpointLocatorRepository.getAdminEndpoint();
        Assert.assertNotNull(endpoint);
    }

    @Test
    @TestCaseName("Run testGetAdminDataInterChangeEndpoint for getAdminDataInterChangeEndpoint()")
    public void testGetAdminDataInterChangeEndpoint() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        @NotNull final AdminDataInterChangeEndpoint endpoint = this.endpointLocatorRepository.getAdminDataInterChangeEndpoint();
        Assert.assertNotNull(endpoint);
    }

    @Test
    @TestCaseName("Run testGetSessionEndpoint for getSessionEndpoint()")
    public void testGetSessionEndpoint() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        @NotNull final SessionEndpoint endpoint = this.endpointLocatorRepository.getSessionEndpoint();
        Assert.assertNotNull(endpoint);
    }

    @Test
    @TestCaseName("Run testGetUserEndpoint for getUserEndpoint()")
    public void testGetUserEndpoint() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        @NotNull final UserEndpoint endpoint = this.endpointLocatorRepository.getUserEndpoint();
        Assert.assertNotNull(endpoint);
    }

    @Test
    @TestCaseName("Run testGetProjectEndpoint for getProjectEndpoint()")
    public void testGetProjectEndpoint() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        @NotNull final ProjectEndpoint endpoint = this.endpointLocatorRepository.getProjectEndpoint();
        Assert.assertNotNull(endpoint);
    }

    @Test
    @TestCaseName("Run testGetTaskEndpoint for getTaskEndpoint()")
    public void testGetTaskEndpoint() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        @NotNull final TaskEndpoint endpoint = this.endpointLocatorRepository.getTaskEndpoint();
        Assert.assertNotNull(endpoint);
    }

}