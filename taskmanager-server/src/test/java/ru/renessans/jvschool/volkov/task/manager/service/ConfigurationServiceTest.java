package ru.renessans.jvschool.volkov.task.manager.service;

import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.renessans.jvschool.volkov.task.manager.api.service.IConfigurationService;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.ServiceImplementation;

public final class ConfigurationServiceTest {

    @NotNull
    private final IConfigurationService configService = new ConfigurationService();

    @Before
    public void loadConfigBefore() {
        Assert.assertNotNull(this.configService);
        this.configService.load();
    }

    @Test
    @TestCaseName("Run testGetServerHost for getServerHost()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetServerHost() {
        Assert.assertNotNull(this.configService);
        @NotNull final String host = this.configService.getServerHost();
        Assert.assertNotNull(host);
    }

    @Test
    @TestCaseName("Run testGetServerPort for getServerPort()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetServerPort() {
        Assert.assertNotNull(this.configService);
        @NotNull final Integer port = this.configService.getServerPort();
        Assert.assertNotNull(port);
    }

    @Test
    @TestCaseName("Run testGetSessionSalt for getSessionSalt()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetSessionSalt() {
        Assert.assertNotNull(this.configService);
        @NotNull final String salt = this.configService.getSessionSalt();
        Assert.assertNotNull(salt);
    }

    @Test
    @TestCaseName("Run testGetSessionCycle for getSessionCycle()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetSessionCycle() {
        Assert.assertNotNull(this.configService);
        @NotNull final Integer cycle = this.configService.getSessionCycle();
        Assert.assertNotNull(cycle);
    }

    @Test
    @TestCaseName("Run testGetBinFileName for getBinFileName()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetBinFileName() {
        Assert.assertNotNull(this.configService);
        @NotNull final String binFileName = this.configService.getBinPathname();
        Assert.assertNotNull(binFileName);
    }

    @Test
    @TestCaseName("Run testGetBase64FileName for getBase64FileName()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetBase64FileName() {
        Assert.assertNotNull(this.configService);
        @NotNull final String base64FileName = this.configService.getBase64Pathname();
        Assert.assertNotNull(base64FileName);
    }

    @Test
    @TestCaseName("Run testGetJsonFileName for getJsonFileName()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetJsonFileName() {
        Assert.assertNotNull(this.configService);
        @NotNull final String jsonFileName = this.configService.getJsonPathname();
        Assert.assertNotNull(jsonFileName);
    }

    @Test
    @TestCaseName("Run testGetXmlFileName for getXmlFileName()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetXmlFileName() {
        Assert.assertNotNull(this.configService);
        @NotNull final String xmlFileName = this.configService.getXmlPathname();
        Assert.assertNotNull(xmlFileName);
    }

    @Test
    @TestCaseName("Run testGetYamlFileName for getYamlFileName()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetYamlFileName() {
        Assert.assertNotNull(this.configService);
        @NotNull final String yamlFileName = this.configService.getYamlPathname();
        Assert.assertNotNull(yamlFileName);
    }

}