package ru.renessans.jvschool.volkov.task.manager.casedata;

public final class CaseDataBaseProvider {

    @SuppressWarnings("unused")
    public Object[] invalidObjectCaseData() {
        return new Object[]{
                invalidLinesCaseData()[0]
        };
    }

    @SuppressWarnings("unused")
    public Object[] invalidLinesCaseData() {
        return new Object[]{
                new Object[]{null},
                new Object[]{"    "},
                new Object[]{""}
        };
    }

    @SuppressWarnings("unused")
    public Object[] validLinesCaseData() {
        return new Object[]{
                new Object[]{"fg6A0Induv"},
                new Object[]{"JyWYAYmqJf"},
                new Object[]{"nnerVw24PV"}
        };
    }

}