package ru.renessans.jvschool.volkov.task.manager.repository;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IUserRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.IConfigurationService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEntityManagerFactoryService;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataUserProvider;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidUserException;
import ru.renessans.jvschool.volkov.task.manager.marker.NegativeImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.RepositoryImplementation;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.service.ConfigurationService;
import ru.renessans.jvschool.volkov.task.manager.service.EntityManagerFactoryService;

import javax.persistence.EntityManager;

@RunWith(value = JUnitParamsRunner.class)
public final class UserRepositoryTest {

    @NotNull
    private final IConfigurationService configService = new ConfigurationService();

    @NotNull
    private final IEntityManagerFactoryService entityManagerFactoryService = new EntityManagerFactoryService(configService);

//    @Before
//    public void loadConfigurationBefore() {
//        Assert.assertNotNull(this.configService);
//        this.configService.load();
//        this.entityManagerFactoryService.build();
//    }
//
//    @Test(expected = InvalidUserException.class)
//    @TestCaseName("Run testNegativeDeleteByLogin for deleteByLogin({0})")
//    @Category({NegativeImplementation.class, RepositoryImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validUsersCaseData"
//    )
//
//    public void testNegativeDeleteByLogin(
//            @NotNull final User user
//    ) {
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(this.entityManagerFactoryService);
//        @NotNull final EntityManager entityManager = this.entityManagerFactoryService.getEntityManager();
//        Assert.assertNotNull(entityManager);
//        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
//        Assert.assertNotNull(userRepository);
//        userRepository.deleteByLogin(user.getLogin());
//    }
//
//    @Test
//    @TestCaseName("Run testGetByLogin for getByLogin({0})")
//    @Category({PositiveImplementation.class, RepositoryImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validUsersCaseData"
//    )
//    public void testGetByLogin(
//            @NotNull final User user
//    ) {
//        Assert.assertNotNull(this.entityManagerFactoryService);
//        Assert.assertNotNull(user);
//        @NotNull final EntityManager entityManager = this.entityManagerFactoryService.getEntityManager();
//        Assert.assertNotNull(entityManager);
//        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
//        Assert.assertNotNull(userRepository);
//        @NotNull final User addRecord = userRepository.persist(user);
//        Assert.assertNotNull(addRecord);
//
//        @Nullable final User getUser = userRepository.getByLogin(addRecord.getLogin());
//        Assert.assertNotNull(getUser);
//        Assert.assertEquals(user.getId(), getUser.getId());
//        Assert.assertEquals(user.getLogin(), getUser.getLogin());
//        Assert.assertEquals(user.getPasswordHash(), getUser.getPasswordHash());
//        Assert.assertEquals(user.getRole(), getUser.getRole());
//    }
//
//    @Test
//    @TestCaseName("Run testDeleteByLogin for deleteByLogin({0})")
//    @Category({PositiveImplementation.class, RepositoryImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validUsersCaseData"
//    )
//    public void testDeleteByLogin(
//            @NotNull final User user
//    ) {
//        Assert.assertNotNull(this.entityManagerFactoryService);
//        Assert.assertNotNull(user);
//        @NotNull final EntityManager entityManager = this.entityManagerFactoryService.getEntityManager();
//        Assert.assertNotNull(entityManager);
//        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
//        Assert.assertNotNull(userRepository);
//        @NotNull final User addRecord = userRepository.persist(user);
//        Assert.assertNotNull(addRecord);
//
//        @Nullable final User deleteUser = userRepository.deleteByLogin(user.getLogin());
//        Assert.assertNotNull(deleteUser);
//        Assert.assertEquals(user.getId(), deleteUser.getId());
//        Assert.assertEquals(user.getLogin(), deleteUser.getLogin());
//        Assert.assertEquals(user.getPasswordHash(), deleteUser.getPasswordHash());
//        Assert.assertEquals(user.getRole(), deleteUser.getRole());
//    }

}