package ru.renessans.jvschool.volkov.task.manager.endpoint;

import junitparams.JUnitParamsRunner;
import org.jetbrains.annotations.NotNull;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.ISessionAdapterService;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.ISessionEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.*;
import ru.renessans.jvschool.volkov.task.manager.marker.EndpointImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.repository.ServiceLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.service.ServiceLocatorService;

@RunWith(value = JUnitParamsRunner.class)
@Category({PositiveImplementation.class, EndpointImplementation.class})
public final class SessionEndpointTest {

    @NotNull
    private final IServiceLocatorRepository serviceLocatorRepository = new ServiceLocatorRepository();

    @NotNull
    private final IServiceLocatorService serviceLocator = new ServiceLocatorService(serviceLocatorRepository);

    @NotNull
    private final IAuthenticationService authService = serviceLocator.getAuthenticationService();

    @NotNull
    private final IUserService userService = serviceLocator.getUserService();

    @NotNull
    private final IConfigurationService configService = serviceLocator.getConfigurationService();

    @NotNull
    private final ISessionService sessionService = serviceLocator.getSessionService();

    @NotNull
    private final IAdapterLocatorService adapterService = serviceLocator.getAdapterService();

    @NotNull
    private final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(serviceLocator);

//    @Before
//    public void loadConfigurationBefore() {
//        Assert.assertNotNull(this.configService);
//        this.configService.load();
//        this.serviceLocator.getEntityManagerService().build();
//    }
//
//    @Test
//    @TestCaseName("Run testOpenSession for openSession(\"{0}\",\"{1}\")")
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validUsersMainFieldsCaseData"
//    )
//    public void testOpenSession(
//            @NotNull final String login,
//            @NotNull final String password
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.sessionEndpoint);
//        Assert.assertNotNull(login);
//        Assert.assertNotNull(password);
//        @NotNull final User addUser = this.userService.addUser(login, password);
//        Assert.assertNotNull(addUser);
//
//        @Nullable final SessionDTO open = this.sessionEndpoint.openSession(login, password);
//        Assert.assertNotNull(open);
//        Assert.assertEquals(addUser.getId(), open.getUserId());
//        @Nullable final Session conversion = this.sessionAdapter.toModel(open);
//        Assert.assertNotNull(conversion);
//        @Nullable final Session getSession = this.sessionService.getSessionByUserId(conversion);
//        Assert.assertNotNull(getSession);
//        Assert.assertEquals(open.getId(), getSession.getId());
//    }
//
//    @Test
//    @TestCaseName("Run testCloseSession for closeSession({1})")
//    @Parameters(
//            source = CaseDataSessionProvider.class,
//            method = "validSessionsCaseData"
//    )
//    public void testCloseSession(
//            @NotNull final User user,
//            @NotNull final Session session
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.sessionEndpoint);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(session);
//        @NotNull final User addUser = this.userService.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addUser);
//        @NotNull final Session open = this.sessionService.openSession(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        final boolean isClosedSession = this.sessionEndpoint.closeSession(conversion);
//        Assert.assertTrue(isClosedSession);
//    }
//
//    @Test
//    @TestCaseName("Run testValidateSession for validateSession({1})")
//    @Parameters(
//            source = CaseDataSessionProvider.class,
//            method = "validSessionsCaseData"
//    )
//    public void testValidateSession(
//            @NotNull final User user,
//            @NotNull final Session session
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.sessionEndpoint);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(session);
//        @NotNull final User addUser = this.userService.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addUser);
//        @NotNull final Session open = this.sessionService.openSession(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @Nullable final SessionDTO validate = this.sessionEndpoint.validateSession(conversion);
//        Assert.assertNotNull(validate);
//        Assert.assertEquals(open.getId(), validate.getId());
//        Assert.assertEquals(open.getUserId(), validate.getUserId());
//        Assert.assertEquals(open.getTimestamp(), validate.getTimestamp());
//        Assert.assertEquals(open.getSignature(), validate.getSignature());
//    }
//
//    @Test
//    @TestCaseName("Run testVerifyValidUserData for validateSession({1}, role)")
//    @Parameters(
//            source = CaseDataSessionProvider.class,
//            method = "validSessionsCaseData"
//    )
//    public void testValidateSessionWithCommandRole(
//            @NotNull final User user,
//            @NotNull final Session session
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.sessionEndpoint);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(session);
//        @NotNull final User addUser = this.userService.addUser(user.getLogin(), user.getPasswordHash(), user.getRole());
//        Assert.assertNotNull(addUser);
//        @NotNull final Session open = this.sessionService.openSession(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @Nullable final SessionDTO validate = this.sessionEndpoint.validateSessionWithCommandRole(conversion, user.getRole());
//        Assert.assertNotNull(validate);
//        Assert.assertEquals(open.getId(), validate.getId());
//        Assert.assertEquals(open.getUserId(), validate.getUserId());
//        Assert.assertEquals(open.getTimestamp(), validate.getTimestamp());
//        Assert.assertEquals(open.getSignature(), validate.getSignature());
//    }
//
//    @Test
//    @TestCaseName("Run testVerifyValidSessionState for validateSession({1})")
//    @Parameters(
//            source = CaseDataSessionProvider.class,
//            method = "validSessionsCaseData"
//    )
//    public void testVerifyValidSessionState(
//            @NotNull final User user,
//            @NotNull final Session session
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.sessionEndpoint);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(session);
//        @NotNull final User addUser = this.userService.addUser(user.getLogin(), user.getPasswordHash(), user.getRole());
//        Assert.assertNotNull(addUser);
//        @NotNull final Session open = this.sessionService.openSession(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @NotNull final SessionValidState verifyValidSessionState = this.sessionEndpoint.verifyValidSessionState(conversion);
//        Assert.assertNotNull(verifyValidSessionState);
//        Assert.assertEquals(SessionValidState.SUCCESS, verifyValidSessionState);
//    }
//
//    @Test
//    @TestCaseName("Run testVerifyValidPermissionState for verifyValidPermission(\"{0}\", \"{1}\")")
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validUsersMainFieldsCaseData"
//    )
//    public void testVerifyValidPermissionState(
//            @NotNull final String login,
//            @NotNull final String password
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.authService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.sessionEndpoint);
//        Assert.assertNotNull(login);
//        Assert.assertNotNull(password);
//        @NotNull final User addUser = this.userService.addUser(login, password);
//        Assert.assertNotNull(addUser);
//        @NotNull final Session open = this.sessionService.openSession(login, password);
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @NotNull final PermissionValidState permissionValidState =
//                this.sessionEndpoint.verifyValidPermissionState(conversion, addUser.getRole());
//        Assert.assertNotNull(permissionValidState);
//        Assert.assertEquals(PermissionValidState.SUCCESS, permissionValidState);
//    }

}