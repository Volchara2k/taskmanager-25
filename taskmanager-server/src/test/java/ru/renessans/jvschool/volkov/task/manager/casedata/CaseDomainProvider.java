package ru.renessans.jvschool.volkov.task.manager.casedata;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.dto.DomainDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.UserDTO;

import java.util.Arrays;
import java.util.Collections;

public final class CaseDomainProvider {

    @SuppressWarnings("unused")
    public Object[] validCollectionDomainsCaseData() {
        @NotNull final DomainDTO domainTestCase1st = new DomainDTO();
        @NotNull final ProjectDTO projectTestCase1st = new ProjectDTO();
        projectTestCase1st.setTitle("demo");
        projectTestCase1st.setDescription("fg6A0Induv");
        @NotNull final TaskDTO taskTestCase1st = new TaskDTO();
        taskTestCase1st.setTitle(".....");
        taskTestCase1st.setDescription("JyWYAYmqJf");
        @NotNull final UserDTO userTestCase1st = new UserDTO();
        userTestCase1st.setLogin("!23");
        userTestCase1st.setPasswordHash("nnerVw24PV");
        domainTestCase1st.setProjects(
                Arrays.asList(
                        projectTestCase1st,
                        new ProjectDTO()
                )
        );
        domainTestCase1st.setTasks(
                Arrays.asList(
                        taskTestCase1st,
                        new TaskDTO()
                )
        );
        domainTestCase1st.setUsers(
                Arrays.asList(
                        userTestCase1st,
                        new UserDTO()
                )
        );

        @NotNull final DomainDTO domainTestCase2nd = new DomainDTO();
        @NotNull final ProjectDTO projectTestCase2nd = new ProjectDTO();
        projectTestCase1st.setTitle("fg6A0Induv");
        projectTestCase1st.setDescription("fg6A0Induv");
        @NotNull final TaskDTO taskTestCase2nd = new TaskDTO();
        taskTestCase1st.setTitle("JyWYAYmqJf");
        taskTestCase1st.setDescription("JyWYAYmqJf");
        @NotNull final UserDTO userTestCase2nd = new UserDTO();
        userTestCase1st.setLogin("nnerVw24PV");
        userTestCase1st.setPasswordHash("nnerVw24PV");
        domainTestCase2nd.setProjects(
                Collections.singletonList(
                        projectTestCase2nd
                )
        );
        domainTestCase2nd.setTasks(
                Collections.singletonList(
                        taskTestCase2nd
                )
        );
        domainTestCase2nd.setUsers(
                Collections.singletonList(
                        userTestCase2nd
                )
        );

        @NotNull final DomainDTO domainTestCase3rd = new DomainDTO();
        @NotNull final ProjectDTO projectTestCase3rd = new ProjectDTO();
        projectTestCase1st.setTitle(DemoDataConst.PROJECT_TITLE);
        projectTestCase1st.setDescription(DemoDataConst.PROJECT_DESCRIPTION);
        @NotNull final TaskDTO taskTestCase3rd = new TaskDTO();
        taskTestCase1st.setTitle(DemoDataConst.TASK_TITLE);
        taskTestCase1st.setDescription(DemoDataConst.TASK_DESCRIPTION);
        @NotNull final UserDTO userTestCase3rd = new UserDTO();
        userTestCase1st.setLogin("nnerVw24PV");
        userTestCase1st.setPasswordHash("nnerVw24PV");
        domainTestCase3rd.setProjects(
                Collections.singletonList(
                        projectTestCase3rd
                )
        );
        domainTestCase3rd.setTasks(
                Collections.singletonList(
                        taskTestCase3rd

                )
        );
        domainTestCase3rd.setUsers(
                Collections.singletonList(
                        userTestCase3rd
                )
        );

        return new Object[]{
                new Object[]{
                        domainTestCase1st
                },
                new Object[]{
                        domainTestCase2nd
                },
                new Object[]{
                        domainTestCase3rd
                }
        };
    }

}