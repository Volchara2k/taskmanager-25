package ru.renessans.jvschool.volkov.task.manager.service;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IUserRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.IAuthenticationService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IConfigurationService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEntityManagerFactoryService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataUserProvider;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.enumeration.PermissionValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserDataValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRole;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidFirstNameException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidLoginException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidPasswordException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidUserRoleException;
import ru.renessans.jvschool.volkov.task.manager.marker.NegativeImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.ServiceImplementation;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.repository.UserRepository;
import ru.renessans.jvschool.volkov.task.manager.util.HashUtil;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.assertThrows;

@RunWith(value = JUnitParamsRunner.class)
public final class AuthenticationServiceTest {

    @NotNull
    private final IConfigurationService configService = new ConfigurationService();

    @NotNull
    private final IEntityManagerFactoryService entityManagerFactoryService = new EntityManagerFactoryService(configService);

    @NotNull
    private final IUserService userService = new UserService(entityManagerFactoryService);

    @NotNull
    private final IAuthenticationService authService = new AuthenticationService(userService);

//    @Before
//    public void loadConfigurationBefore() {
//        Assert.assertNotNull(this.configService);
//        this.configService.load();
//        this.entityManagerFactoryService.build();
//    }
//
//    @Test
//    @TestCaseName("Run testNegativeVerifyValidUserData for verifyValidUserData({0}, {1})")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "invalidUsersEditableCaseData"
//    )
//    public void testNegativeVerifyValidUserData(
//            @Nullable final String login,
//            @Nullable final String password
//    ) {
//        Assert.assertNotNull(this.authService);
//
//        @NotNull final InvalidLoginException loginThrown = assertThrows(
//                InvalidLoginException.class,
//                () -> this.authService.verifyValidUserData(login, password)
//        );
//        Assert.assertNotNull(loginThrown);
//        Assert.assertNotNull(loginThrown.getMessage());
//
//        @NotNull final String tempLogin = DemoDataConst.USER_DEFAULT_LOGIN;
//        @NotNull final InvalidPasswordException passwordThrown = assertThrows(
//                InvalidPasswordException.class,
//                () -> this.authService.verifyValidUserData(tempLogin, password)
//        );
//        Assert.assertNotNull(passwordThrown);
//        Assert.assertNotNull(passwordThrown.getMessage());
//    }
//
//    @Test
//    @TestCaseName("Run testNegativeSignUp for signUp(\"{0}\", \"{1}\", \"{3}\", {4})")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "invalidUsersAllFieldsCaseData"
//    )
//    public void testNegativeSignUp(
//            @Nullable final String login,
//            @Nullable final String password,
//            @Nullable final String firstName,
//            @Nullable final UserRole userRole
//    ) {
//        Assert.assertNotNull(this.authService);
//
//        @NotNull final InvalidLoginException loginThrown = assertThrows(
//                InvalidLoginException.class,
//                () -> this.authService.signUp(login, password)
//        );
//        Assert.assertNotNull(loginThrown);
//        Assert.assertNotNull(loginThrown.getMessage());
//
//        @NotNull final String tempLogin = DemoDataConst.USER_DEFAULT_LOGIN;
//        @NotNull final InvalidPasswordException passwordThrown = assertThrows(
//                InvalidPasswordException.class,
//                () -> this.authService.signUp(tempLogin, password)
//        );
//        Assert.assertNotNull(passwordThrown);
//        Assert.assertNotNull(passwordThrown.getMessage());
//
//        @NotNull final String tempPassword = DemoDataConst.USER_DEFAULT_PASSWORD;
//        @NotNull final InvalidFirstNameException firstNameThrown = assertThrows(
//                InvalidFirstNameException.class,
//                () -> this.authService.signUp(tempLogin, tempPassword, firstName)
//        );
//        Assert.assertNotNull(firstNameThrown);
//        Assert.assertNotNull(firstNameThrown.getMessage());
//
//        @NotNull final InvalidUserRoleException roleThrown = assertThrows(
//                InvalidUserRoleException.class,
//                () -> this.authService.signUp(tempLogin, tempPassword, userRole)
//        );
//        Assert.assertNotNull(roleThrown);
//        Assert.assertNotNull(roleThrown.getMessage());
//    }
//
//    @Test
//    @TestCaseName("Run testGetUserRole for getUserRole({0})")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validUsersCaseData"
//    )
//    public void testGetUserRole(
//            @NotNull final User user
//    ) {
//        Assert.assertNotNull(this.entityManagerFactoryService);
//        Assert.assertNotNull(this.authService);
//        Assert.assertNotNull(user);
//        @NotNull final EntityManager entityManager = this.entityManagerFactoryService.getEntityManager();
//        Assert.assertNotNull(entityManager);
//        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
//        Assert.assertNotNull(userRepository);
//        @NotNull final User addRecord = userRepository.persist(user);
//        Assert.assertNotNull(addRecord);
//
//        @NotNull final UserRole getRole = this.authService.getUserRole(addRecord.getId());
//        Assert.assertNotNull(getRole);
//        Assert.assertEquals(addRecord.getRole(), getRole);
//    }
//
//    @Test
//    @TestCaseName("Run testVerifyValidPermission for verifyValidPermission({0})")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validUsersCaseData"
//    )
//    public void testVerifyValidPermission(
//            @NotNull final User user
//    ) {
//        Assert.assertNotNull(this.entityManagerFactoryService);
//        Assert.assertNotNull(this.authService);
//        Assert.assertNotNull(user);
//        @NotNull final EntityManager entityManager = this.entityManagerFactoryService.getEntityManager();
//        Assert.assertNotNull(entityManager);
//        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
//        Assert.assertNotNull(userRepository);
//        @NotNull final User addRecord = userRepository.persist(user);
//        Assert.assertNotNull(addRecord);
//
//        @NotNull final PermissionValidState permissionValidState =
//                this.authService.verifyValidPermission(addRecord.getId(), addRecord.getRole());
//        Assert.assertNotNull(permissionValidState);
//        Assert.assertEquals(PermissionValidState.SUCCESS, permissionValidState);
//    }
//
//    @Test
//    @TestCaseName("Run testVerifyValidUserData for verifyValidUserData(\"{0}\", \"{1}\")")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validUsersMainFieldsCaseData"
//    )
//    public void testVerifyValidUserData(
//            @NotNull final String login,
//            @NotNull final String password
//    ) {
//        Assert.assertNotNull(this.entityManagerFactoryService);
//        Assert.assertNotNull(this.authService);
//        Assert.assertNotNull(login);
//        Assert.assertNotNull(password);
//        @NotNull final User addRecord = this.authService.signUp(login, password);
//        Assert.assertNotNull(addRecord);
//
//        @NotNull final UserDataValidState userDataValidState = this.authService.verifyValidUserData(login, password);
//        Assert.assertNotNull(userDataValidState);
//        Assert.assertEquals(UserDataValidState.SUCCESS, userDataValidState);
//    }
//
//    @Test
//    @TestCaseName("Run testSignUp for verifyValidUserData(\"{0}\", \"{1}\")")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validUsersMainFieldsCaseData"
//    )
//    public void testSignUp(
//            @NotNull final String login,
//            @NotNull final String password
//    ) {
//        Assert.assertNotNull(this.entityManagerFactoryService);
//        Assert.assertNotNull(this.authService);
//        Assert.assertNotNull(login);
//        Assert.assertNotNull(password);
//
//        @NotNull final User addUser = this.authService.signUp(login, password);
//        Assert.assertNotNull(addUser);
//        Assert.assertEquals(addUser.getId(), addUser.getId());
//        Assert.assertEquals(login, addUser.getLogin());
//        @NotNull final String hashPassword = HashUtil.getSaltHashLine(password);
//        Assert.assertEquals(hashPassword, addUser.getPasswordHash());
//        Assert.assertEquals(addUser.getRole(), addUser.getRole());
//    }
//
//    @Test
//    @TestCaseName("Run testSignUpWithUserRole for verifyValidUserData(\"{0}\", \"{1}\", {2})")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validUsersMainFieldsWithRoleCaseData"
//    )
//    public void testSignUpWithUserRole(
//            @NotNull final String login,
//            @NotNull final String password,
//            @NotNull final UserRole userRole
//    ) {
//        Assert.assertNotNull(this.entityManagerFactoryService);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(login);
//        Assert.assertNotNull(password);
//
//        @NotNull final User addUser = this.userService.addUser(login, password, userRole);
//        Assert.assertNotNull(addUser);
//        Assert.assertEquals(login, addUser.getLogin());
//        @NotNull final String hashPassword = HashUtil.getSaltHashLine(password);
//        Assert.assertEquals(hashPassword, addUser.getPasswordHash());
//        Assert.assertEquals(userRole, addUser.getRole());
//    }

}