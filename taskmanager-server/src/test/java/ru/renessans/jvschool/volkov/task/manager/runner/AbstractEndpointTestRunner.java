package ru.renessans.jvschool.volkov.task.manager.runner;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import ru.renessans.jvschool.volkov.task.manager.endpoint.*;

@RunWith(Suite.class)
@Suite.SuiteClasses(
        {
                AdminDataInterChangeEndpointTest.class,
                AdminEndpointTest.class,
                AuthenticationEndpointTest.class,
                ProjectEndpointTest.class,
                SessionEndpointTest.class,
                TaskEndpointTest.class,
                UserEndpointTest.class
        }
)

public abstract class AbstractEndpointTestRunner {
}