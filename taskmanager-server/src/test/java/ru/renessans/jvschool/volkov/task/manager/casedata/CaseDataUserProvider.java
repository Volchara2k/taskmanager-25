package ru.renessans.jvschool.volkov.task.manager.casedata;

import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRole;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.util.HashUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public final class CaseDataUserProvider {

    @SuppressWarnings("unused")
    public Object[] validUsersCaseData() {
        return new Object[]{
                new Object[]{
                        new User(
                                DemoDataConst.USER_TEST_LOGIN,
                                DemoDataConst.USER_TEST_PASSWORD
                        )
                },
                new Object[]{
                        new User(
                                HashUtil.getSaltHashLine(DemoDataConst.USER_DEFAULT_LOGIN),
                                HashUtil.getSaltHashLine(DemoDataConst.USER_DEFAULT_PASSWORD),
                                DemoDataConst.USER_DEFAULT_FIRSTNAME
                        )
                },
                new Object[]{
                        new User(
                                HashUtil.getHashLine(DemoDataConst.USER_ADMIN_LOGIN),
                                HashUtil.getHashLine(DemoDataConst.USER_ADMIN_PASSWORD),
                                UserRole.ADMIN
                        )
                }
        };
    }

    @SuppressWarnings("unused")
    public Object[] validCollectionUsersCaseData() {
        return new Object[]{
                new Object[]{
                        Collections.singletonList(
                                new User(
                                        DemoDataConst.USER_TEST_LOGIN,
                                        DemoDataConst.USER_TEST_PASSWORD
                                )
                        )
                },
                new Object[]{
                        Collections.singletonList(
                                new User(
                                        HashUtil.getSaltHashLine(DemoDataConst.USER_DEFAULT_LOGIN),
                                        HashUtil.getSaltHashLine(DemoDataConst.USER_DEFAULT_PASSWORD),
                                        DemoDataConst.USER_DEFAULT_FIRSTNAME
                                )
                        )
                },
                new Object[]{
                        Collections.singletonList(
                                new User(
                                        HashUtil.getHashLine(DemoDataConst.USER_ADMIN_LOGIN),
                                        HashUtil.getHashLine(DemoDataConst.USER_ADMIN_PASSWORD),
                                        UserRole.ADMIN
                                )
                        )
                }
        };
    }

    @SuppressWarnings("unused")
    public Object[] validUsersMainFieldsCaseData() {
        return new Object[]{
                new Object[]{
                        DemoDataConst.USER_TEST_LOGIN,
                        DemoDataConst.USER_TEST_PASSWORD
                },
                new Object[]{
                        HashUtil.getSaltHashLine(DemoDataConst.USER_DEFAULT_LOGIN),
                        HashUtil.getSaltHashLine(DemoDataConst.USER_DEFAULT_PASSWORD)
                },
                new Object[]{
                        HashUtil.getHashLine(DemoDataConst.USER_ADMIN_LOGIN),
                        HashUtil.getHashLine(DemoDataConst.USER_ADMIN_PASSWORD)
                }
        };
    }

    @SuppressWarnings("unused")
    public Object[] validUsersMainFieldsWithNewEntityCaseData() {
        return new Object[]{
                new Object[]{
                        DemoDataConst.USER_TEST_LOGIN,
                        DemoDataConst.USER_TEST_PASSWORD,
                        "fg6A0Induv"
                },
                new Object[]{
                        HashUtil.getSaltHashLine(DemoDataConst.USER_DEFAULT_LOGIN),
                        HashUtil.getSaltHashLine(DemoDataConst.USER_DEFAULT_PASSWORD),
                        "JyWYAYmqJf"
                },
                new Object[]{
                        HashUtil.getHashLine(DemoDataConst.USER_ADMIN_LOGIN),
                        HashUtil.getHashLine(DemoDataConst.USER_ADMIN_PASSWORD),
                        "nnerVw24PV"
                }
        };
    }

    @SuppressWarnings("unused")
    public Object[] validUsersMainFieldsWithRoleCaseData() {
        return new Object[]{
                new Object[]{
                        DemoDataConst.USER_TEST_LOGIN,
                        DemoDataConst.USER_TEST_PASSWORD,
                        UserRole.USER
                },
                new Object[]{
                        DemoDataConst.USER_DEFAULT_LOGIN,
                        DemoDataConst.USER_DEFAULT_PASSWORD,
                        UserRole.USER
                },
                new Object[]{
                        DemoDataConst.USER_ADMIN_LOGIN,
                        DemoDataConst.USER_ADMIN_PASSWORD,
                        UserRole.ADMIN
                }
        };
    }

    @SuppressWarnings("unused")
    public Object[] invalidUsersAllFieldsCaseData() {
        return new Object[]{
                new Object[]{
                        null,
                        null,
                        null,
                        null
                },
                new Object[]{
                        "",
                        "",
                        "",
                        null
                },
                new Object[]{
                        "   ",
                        "   ",
                        "   ",
                        null
                }
        };
    }

    @SuppressWarnings("unused")
    public Object[] invalidUsersEditableCaseData() {
        return new Object[]{
                new Object[]{
                        null,
                        null
                },
                new Object[]{
                        "",
                        ""
                },
                new Object[]{
                        "   ",
                        "   "
                }
        };
    }

    @SuppressWarnings("unused")
    public Object[] invalidOwnerWithIdCaseData() {
        return new Object[]{
                new Object[]{
                        null,
                        null,
                        null,
                        null
                },
                new Object[]{
                        "",
                        "",
                        "",
                        ""
                },
                new Object[]{
                        "   ",
                        "   ",
                        "   ",
                        "   "
                }
        };
    }


    @SuppressWarnings("unused")
    public Object[] invalidOwnerWithIndexCaseData() {
        return new Object[]{
                new Object[]{
                        null,
                        null,
                        null,
                        null
                },
                new Object[]{
                        "",
                        -1,
                        "",
                        ""
                },
                new Object[]{
                        "   ",
                        -5,
                        "   ",
                        "   "
                }
        };
    }

    @SuppressWarnings("unused")
    public Object[] invalidOwnerMainFieldsCaseData() {
        return new Object[]{
                new Object[]{
                        null,
                        null,
                        null
                },
                new Object[]{
                        "",
                        "",
                        ""
                },
                new Object[]{
                        "   ",
                        "   ",
                        "   "
                }
        };
    }

    @SuppressWarnings("unused")
    public Object[] invalidOwnerIndexCaseData() {
        return new Object[]{
                new Object[]{
                        null,
                        null
                },
                new Object[]{
                        "",
                        -1
                },
                new Object[]{
                        "   ",
                        -5
                }
        };
    }

    @SuppressWarnings("unused")
    public Object[] invalidCollectionsUsersCaseData() {
        return new Object[]{
                new Object[]{null},
                new Object[]{
                        Arrays.asList(
                                null,
                                null,
                                null
                        )
                },
                new Object[]{Collections.emptyList()},
                new Object[]{new ArrayList<>()}
        };
    }

}