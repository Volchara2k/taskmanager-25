package ru.renessans.jvschool.volkov.task.manager.service;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IUserRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.*;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataSessionProvider;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataUserProvider;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.enumeration.PermissionValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.SessionValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserDataValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRole;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidLoginException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidPasswordException;
import ru.renessans.jvschool.volkov.task.manager.exception.security.AccessFailureException;
import ru.renessans.jvschool.volkov.task.manager.marker.NegativeImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.ServiceImplementation;
import ru.renessans.jvschool.volkov.task.manager.model.Session;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.repository.UserRepository;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.assertThrows;

@RunWith(value = JUnitParamsRunner.class)
public final class SessionServiceTest {

    @NotNull
    private final IConfigurationService configService = new ConfigurationService();

    @NotNull
    private final IEntityManagerFactoryService entityManagerFactoryService = new EntityManagerFactoryService(configService);

    @NotNull
    private final IUserService userService = new UserService(entityManagerFactoryService);

    @NotNull
    private final IAuthenticationService authService = new AuthenticationService(userService);

    @NotNull
    private final ISessionService sessionService = new SessionService(
            entityManagerFactoryService, authService, userService, configService
    );

//    @Before
//    public void loadConfigBefore() {
//        Assert.assertNotNull(this.configService);
//        this.configService.load();
//        this.entityManagerFactoryService.build();
//    }
//
//    @Test(expected = AccessFailureException.class)
//    @TestCaseName("Run testNegativeSetSignature for setSignature(null)")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    public void testNegativeSetSignature() {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        this.sessionService.setSignature(null);
//    }
//
//    @Test
//    @TestCaseName("Run testNegativeOpenSession for openSession(\"{0}\", \"{1}\")")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "invalidUsersEditableCaseData"
//    )
//    public void testNegativeOpenSession(
//            @Nullable final String login,
//            @Nullable final String password
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//
//        @NotNull final InvalidLoginException loginThrown = assertThrows(
//                InvalidLoginException.class,
//                () -> this.sessionService.openSession(login, password)
//        );
//        Assert.assertNotNull(loginThrown);
//        Assert.assertNotNull(loginThrown.getMessage());
//
//        @NotNull final String tempLogin = DemoDataConst.USER_DEFAULT_LOGIN;
//        @NotNull final InvalidPasswordException passwordThrown = assertThrows(
//                InvalidPasswordException.class,
//                () -> this.sessionService.openSession(tempLogin, password)
//        );
//        Assert.assertNotNull(passwordThrown);
//        Assert.assertNotNull(passwordThrown.getMessage());
//    }
//
//    @Test
//    @TestCaseName("Run testNegativeVerifyValidUserData for verifyValidUserData(\"{0}\", \"{1}\")")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "invalidUsersEditableCaseData"
//    )
//    public void testNegativeValidateUserData(
//            @Nullable final String login,
//            @Nullable final String password
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//
//        @NotNull final InvalidLoginException loginThrown = assertThrows(
//                InvalidLoginException.class,
//                () -> this.sessionService.verifyValidUserData(login, password)
//        );
//        Assert.assertNotNull(loginThrown);
//        Assert.assertNotNull(loginThrown.getMessage());
//
//        @NotNull final String tempLogin = DemoDataConst.USER_DEFAULT_LOGIN;
//        @NotNull final InvalidPasswordException passwordThrown = assertThrows(
//                InvalidPasswordException.class,
//                () -> this.sessionService.verifyValidUserData(tempLogin, password)
//        );
//        Assert.assertNotNull(passwordThrown);
//        Assert.assertNotNull(passwordThrown.getMessage());
//    }
//
//    @Test(expected = AccessFailureException.class)
//    @TestCaseName("Run testNegativeValidateSession for validateSession(null)")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    public void testNegativeValidateSession() {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        this.sessionService.validateSession(null);
//    }
//
//    @Test
//    @TestCaseName("Run testNegativeValidateSessionWithRole for validateSession({1}, role)")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataSessionProvider.class,
//            method = "validSessionsCaseData"
//    )
//    public void testNegativeValidateSessionWithRole(
//            @NotNull final User user,
//            @NotNull final Session session
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(session);
//        @NotNull final Session addRecord = this.sessionService.persist(session);
//        Assert.assertNotNull(addRecord);
//
//        @NotNull final AccessFailureException thrownValidSessionState = assertThrows(
//                AccessFailureException.class,
//                () -> this.sessionService.validateSession(session, UserRole.UNKNOWN)
//        );
//        Assert.assertNotNull(thrownValidSessionState);
//        Assert.assertNotNull(thrownValidSessionState.getMessage());
//
//        @NotNull final Session signatureSession = this.sessionService.setSignature(session);
//        Assert.assertNotNull(signatureSession);
//        @NotNull final EntityManager entityManager = this.entityManagerFactoryService.getEntityManager();
//        Assert.assertNotNull(entityManager);
//        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
//        Assert.assertNotNull(userRepository);
//        @NotNull final User addUser = userRepository.persist(user);
//        Assert.assertNotNull(addUser);
//        @NotNull final AccessFailureException thrownValidPermissionState = assertThrows(
//                AccessFailureException.class,
//                () -> this.sessionService.validateSession(signatureSession, UserRole.UNKNOWN)
//        );
//        Assert.assertNotNull(thrownValidPermissionState);
//        Assert.assertNotNull(thrownValidPermissionState.getMessage());
//    }
//
//    @Test
//    @TestCaseName("Run testSetSignature for setSignature({1})")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataSessionProvider.class,
//            method = "validSessionsCaseData"
//    )
//    public void testSetSignature(
//            @NotNull final User user,
//            @NotNull final Session session
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(session);
//
//        @NotNull final Session signatureSession = this.sessionService.setSignature(session);
//        Assert.assertNotNull(signatureSession);
//        Assert.assertEquals(session.getId(), signatureSession.getId());
//        Assert.assertEquals(session.getUserId(), signatureSession.getUserId());
//        Assert.assertEquals(session.getTimestamp(), signatureSession.getTimestamp());
//        Assert.assertEquals(session.getSignature(), signatureSession.getSignature());
//    }
//
//    @Test
//    @TestCaseName("Run testOpenSession for openSession(\"{0}\",\"{1}\")")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validUsersMainFieldsCaseData"
//    )
//    public void testOpenSession(
//            @NotNull final String login,
//            @NotNull final String password
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(login);
//        Assert.assertNotNull(password);
//        @NotNull final User addUser = this.userService.addUser(login, password);
//        Assert.assertNotNull(addUser);
//
//        @NotNull final Session open = this.sessionService.openSession(login, password);
//        Assert.assertNotNull(open);
//        Assert.assertEquals(addUser.getId(), open.getUserId());
//        @Nullable final Session getSession = this.sessionService.getSessionByUserId(open);
//        Assert.assertNotNull(getSession);
//        Assert.assertEquals(open.getId(), getSession.getId());
//        Assert.assertEquals(open.getUserId(), getSession.getUserId());
//        Assert.assertEquals(open.getTimestamp(), getSession.getTimestamp());
//        Assert.assertEquals(open.getSignature(), getSession.getSignature());
//    }
//
//    @Test
//    @TestCaseName("Run testCloseSession for closeSession({1})")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataSessionProvider.class,
//            method = "validSessionsCaseData"
//    )
//    public void testCloseSession(
//            @NotNull final User user,
//            @NotNull final Session session
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(session);
//        @NotNull final User addUser = this.userService.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addUser);
//        @NotNull final Session open = this.sessionService.openSession(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(open);
//
//        final boolean isClosedSession = this.sessionService.closeSession(open);
//        Assert.assertTrue(isClosedSession);
//    }
//
//    @Test
//    @TestCaseName("Run testCloseAllSessions for closeAllSessions({1})")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataSessionProvider.class,
//            method = "validSessionsCaseData"
//    )
//    public void testCloseAllSessions(
//            @NotNull final User user,
//            @NotNull final Session session
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(session);
//        @NotNull final User addUser = this.userService.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addUser);
//        @NotNull final Session open = this.sessionService.openSession(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(open);
//
//        final boolean isClosedSession = this.sessionService.closeAllSessions(open);
//        Assert.assertTrue(isClosedSession);
//    }
//
//    @Test
//    @TestCaseName("Run testGetSessionByUserId for getSessionByUserId({1})")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataSessionProvider.class,
//            method = "validSessionsCaseData"
//    )
//    public void testGetSessionByUserId(
//            @NotNull final User user,
//            @NotNull final Session session
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(session);
//        @NotNull final User addUser = this.userService.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addUser);
//        @NotNull final Session open = this.sessionService.openSession(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(open);
//
//        @Nullable final Session getSession = this.sessionService.getSessionByUserId(open);
//        Assert.assertNotNull(getSession);
//        Assert.assertEquals(open.getId(), getSession.getId());
//        Assert.assertEquals(open.getUserId(), getSession.getUserId());
//        Assert.assertEquals(open.getTimestamp(), getSession.getTimestamp());
//        Assert.assertEquals(open.getSignature(), getSession.getSignature());
//    }
//
////    @Test
////    @TestCaseName("Run testDeleteSessionByUserId for deleteSessionByUserId({1})")
////    @Category({PositiveImplementation.class, ServiceImplementation.class})
////    @Parameters(
////            source = CaseDataSessionProvider.class,
////            method = "validSessionsCaseData"
////    )
////    public void testDeleteSessionByUserId(
////            @NotNull final User user,
////            @NotNull final Session session
////    ) {
////        Assert.assertNotNull(this.userService);
////        Assert.assertNotNull(this.sessionService);
////        Assert.assertNotNull(user);
////        Assert.assertNotNull(session);
////        @NotNull final User addUser = this.userService.addUser(user.getLogin(), user.getPasswordHash());
////        Assert.assertNotNull(addUser);
////        @NotNull final Session open = this.sessionService.openSession(user.getLogin(), user.getPasswordHash());
////        Assert.assertNotNull(open);
////
////        final boolean deleteSession = this.sessionService.deleteSessionByUserId(open);
////        Assert.assertTrue(deleteSession);
////    }
//
//    @Test
//    @TestCaseName("Run testValidateUserData for verifyValidUserData(\"{0}\", \"{1}\"")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validUsersMainFieldsCaseData"
//    )
//    public void testValidateUserData(
//            @NotNull final String login,
//            @NotNull final String password
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(login);
//        Assert.assertNotNull(password);
//        @NotNull final User addUser = this.userService.addUser(login, password);
//        Assert.assertNotNull(addUser);
//
//        @NotNull final User validate = this.sessionService.validateUserData(login, password);
//        Assert.assertNotNull(validate);
//        Assert.assertEquals(addUser.getId(), validate.getId());
//        Assert.assertEquals(addUser.getLogin(), validate.getLogin());
//        Assert.assertEquals(addUser.getPasswordHash(), validate.getPasswordHash());
//        Assert.assertEquals(addUser.getRole(), validate.getRole());
//    }
//
//    @Test
//    @TestCaseName("Run testVerifyValidUserData for verifyValidUserData(\"{0}\", \"{1}\"")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validUsersMainFieldsCaseData"
//    )
//    public void testVerifyValidUserData(
//            @NotNull final String login,
//            @NotNull final String password
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(login);
//        Assert.assertNotNull(password);
//        @NotNull final User addUser = this.userService.addUser(login, password);
//        Assert.assertNotNull(addUser);
//
//        @NotNull final UserDataValidState userDataValidState = this.sessionService.verifyValidUserData(login, password);
//        Assert.assertNotNull(userDataValidState);
//        Assert.assertEquals(UserDataValidState.SUCCESS, userDataValidState);
//    }
//
//    @Test
//    @TestCaseName("Run testValidateSession for validateSession({1})")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataSessionProvider.class,
//            method = "validSessionsCaseData"
//    )
//    public void testValidateSession(
//            @NotNull final User user,
//            @NotNull final Session session
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(session);
//        @NotNull final Session signatureSession = this.sessionService.setSignature(session);
//        Assert.assertNotNull(signatureSession);
//        @NotNull final Session open = this.sessionService.persist(signatureSession);
//        Assert.assertNotNull(open);
//
//        @NotNull final Session validate = this.sessionService.validateSession(signatureSession);
//        Assert.assertNotNull(validate);
//        Assert.assertEquals(open.getId(), validate.getId());
//        Assert.assertEquals(open.getUserId(), validate.getUserId());
//        Assert.assertEquals(open.getTimestamp(), validate.getTimestamp());
//        Assert.assertEquals(open.getSignature(), validate.getSignature());
//    }
//
//    @Test
//    @TestCaseName("Run testVerifyValidUserData for validateSession({1}, role)")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataSessionProvider.class,
//            method = "validSessionsCaseData"
//    )
//    public void testValidateSessionWithRole(
//            @NotNull final User user,
//            @NotNull final Session session
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(session);
//        @NotNull final Session signatureSession = this.sessionService.setSignature(session);
//        Assert.assertNotNull(signatureSession);
//        @NotNull final EntityManager entityManager = this.entityManagerFactoryService.getEntityManager();
//        Assert.assertNotNull(entityManager);
//        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
//        Assert.assertNotNull(userRepository);
//        @NotNull final User addUser = userRepository.persist(user);
//        Assert.assertNotNull(addUser);
//        @NotNull final Session open = this.sessionService.persist(signatureSession);
//        Assert.assertNotNull(open);
//
//        @NotNull final Session validate = this.sessionService.validateSession(signatureSession, user.getRole());
//        Assert.assertNotNull(validate);
//        Assert.assertEquals(open.getId(), validate.getId());
//        Assert.assertEquals(open.getUserId(), validate.getUserId());
//        Assert.assertEquals(open.getTimestamp(), validate.getTimestamp());
//        Assert.assertEquals(open.getSignature(), validate.getSignature());
//    }
//
//    @Test
//    @TestCaseName("Run testVerifyValidSessionState for validateSession({1})")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataSessionProvider.class,
//            method = "validSessionsCaseData"
//    )
//    public void testVerifyValidSessionState(
//            @NotNull final User user,
//            @NotNull final Session session
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(session);
//        @NotNull final Session signatureSession = this.sessionService.setSignature(session);
//        Assert.assertNotNull(signatureSession);
//        @NotNull final Session open = this.sessionService.persist(signatureSession);
//        Assert.assertNotNull(open);
//
//        @NotNull final SessionValidState verifyValidSessionState = this.sessionService.verifyValidSessionState(open);
//        Assert.assertNotNull(verifyValidSessionState);
//        Assert.assertEquals(SessionValidState.SUCCESS, verifyValidSessionState);
//    }
//
//    @Test
//    @TestCaseName("Run testVerifyValidPermissionState for verifyValidPermission(\"{0}\", \"{1}\")")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validUsersMainFieldsCaseData"
//    )
//    public void testVerifyValidPermissionState(
//            @NotNull final String login,
//            @NotNull final String password
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.authService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(login);
//        Assert.assertNotNull(password);
//        @NotNull final User addUser = this.userService.addUser(login, password);
//        Assert.assertNotNull(addUser);
//
//        @NotNull final PermissionValidState permissionValidState =
//                this.authService.verifyValidPermission(addUser.getId(), addUser.getRole());
//        Assert.assertNotNull(permissionValidState);
//        Assert.assertEquals(PermissionValidState.SUCCESS, permissionValidState);
//    }

}