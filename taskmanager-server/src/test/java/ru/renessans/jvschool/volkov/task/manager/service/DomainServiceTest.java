package ru.renessans.jvschool.volkov.task.manager.service;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.IProjectAdapterService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.ITaskAdapterService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.IUserAdapterService;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IAdapterRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.*;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDomainProvider;
import ru.renessans.jvschool.volkov.task.manager.dto.DomainDTO;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.ServiceImplementation;
import ru.renessans.jvschool.volkov.task.manager.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.UserDTO;
import ru.renessans.jvschool.volkov.task.manager.model.Project;
import ru.renessans.jvschool.volkov.task.manager.model.Task;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.repository.AdapterRepository;
import ru.renessans.jvschool.volkov.task.manager.service.adapter.AdapterLocatorService;

import java.util.Collection;
import java.util.stream.Collectors;

@RunWith(value = JUnitParamsRunner.class)
public final class DomainServiceTest {

    @NotNull
    private final IConfigurationService configService = new ConfigurationService();

    @NotNull
    private final IEntityManagerFactoryService entityManagerFactoryService = new EntityManagerFactoryService(configService);

    @NotNull
    private final IUserService userService = new UserService(entityManagerFactoryService);

    @NotNull
    private final ITaskUserService taskService = new TaskUserService(entityManagerFactoryService);

    @NotNull
    private final IProjectUserService projectService = new ProjectUserService(entityManagerFactoryService);

    @NotNull
    private final IAdapterRepository adapterRepository = new AdapterRepository();

    @NotNull
    private final IAdapterLocatorService adapterService = new AdapterLocatorService(adapterRepository);

    @NotNull
    private final IUserAdapterService userAdapter = adapterService.getUserAdapter();

    @NotNull
    private final ITaskAdapterService taskAdapter = adapterService.getTaskAdapter();

    @NotNull
    private final IProjectAdapterService projectAdapter = adapterRepository.getProjectAdapter();

    @NotNull
    private final IDomainService domainService = new DomainService(
            userService, taskService, projectService, adapterService
    );

//    @Before
//    public void loadConfigurationBefore() {
//        Assert.assertNotNull(this.configService);
//        this.configService.load();
//        this.entityManagerFactoryService.build();
//    }
//
//    @Test
//    @TestCaseName("Run testDataImport for dataImport({0})")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDomainProvider.class,
//            method = "validCollectionDomainsCaseData"
//    )
//    public void testDataImport(
//            @NotNull final DomainDTO data
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.adapterRepository);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.userAdapter);
//        Assert.assertNotNull(this.taskAdapter);
//        Assert.assertNotNull(this.projectAdapter);
//        Assert.assertNotNull(this.taskService);
//        Assert.assertNotNull(this.projectService);
//        Assert.assertNotNull(this.domainService);
//        Assert.assertNotNull(data);
//
//        @Nullable final DomainDTO importDomain = this.domainService.dataImport(data);
//        Assert.assertNotNull(importDomain);
//        @NotNull final Collection<UserDTO> importDomainUsers = importDomain.getUsers();
//        Assert.assertNotNull(importDomainUsers);
//        Assert.assertNotEquals(0, importDomainUsers.size());
//        @NotNull final Collection<User> conversionUsers =
//                importDomainUsers.stream().map(this.userAdapter::toModel).collect(Collectors.toList());
//        Assert.assertNotNull(conversionUsers);
//        @NotNull final Collection<TaskDTO> importDomainTasks = importDomain.getTasks();
//        Assert.assertNotNull(importDomainTasks);
//        Assert.assertNotEquals(0, importDomainTasks.size());
//        @NotNull final Collection<Task> conversionTasks =
//                importDomainTasks.stream().map(this.taskAdapter::toModel).collect(Collectors.toList());
//        Assert.assertNotNull(conversionTasks);
//        @NotNull final Collection<ProjectDTO> importDomainProjects = importDomain.getProjects();
//        Assert.assertNotNull(importDomainProjects);
//        Assert.assertNotEquals(0, importDomainProjects.size());
//        @NotNull final Collection<Project> conversionProjects =
//                importDomainProjects.stream().map(this.projectAdapter::toModel).collect(Collectors.toList());
//        Assert.assertNotNull(conversionProjects);
//        Assert.assertEquals(importDomain.getUsers().toString(), this.userService.getAllRecords().toString());
//        Assert.assertEquals(importDomain.getProjects().toString(), this.projectService.getAllRecords().toString());
//        Assert.assertEquals(importDomain.getTasks().toString(), this.taskService.getAllRecords().toString());
//    }
//
//    @Test
//    @TestCaseName("Run testDataExport for dataExport(domain)")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDomainProvider.class,
//            method = "validCollectionDomainsCaseData"
//    )
//    public void testDataExport(
//            @NotNull final DomainDTO data
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.adapterRepository);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.userAdapter);
//        Assert.assertNotNull(this.taskAdapter);
//        Assert.assertNotNull(this.projectAdapter);
//        Assert.assertNotNull(this.taskService);
//        Assert.assertNotNull(this.projectService);
//        Assert.assertNotNull(this.domainService);
//        Assert.assertNotNull(data);
//        @NotNull final Collection<User> conversionUsers =
//                data.getUsers().stream().map(this.userAdapter::toModel).collect(Collectors.toList());
//        Assert.assertNotNull(conversionUsers);
//        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(conversionUsers);
//        Assert.assertNotNull(setAllUserRecords);
//        @NotNull final Collection<Task> conversionTasks =
//                data.getTasks().stream().map(this.taskAdapter::toModel).collect(Collectors.toList());
//        Assert.assertNotNull(conversionTasks);
//        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(conversionTasks);
//        Assert.assertNotNull(setAllTaskRecords);
//        @NotNull final Collection<Project> conversionProjects =
//                data.getProjects().stream().map(this.projectAdapter::toModel).collect(Collectors.toList());
//        Assert.assertNotNull(conversionProjects);
//        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(conversionProjects);
//        Assert.assertNotNull(setAllProjectRecords);
//
//        @NotNull final DomainDTO domain = new DomainDTO();
//        Assert.assertNotNull(domain);
//        @NotNull final DomainDTO exportDomain = this.domainService.dataExport(domain);
//        Assert.assertNotNull(exportDomain);
//        Assert.assertEquals(this.userService.getAllRecords().toString(), domain.getUsers().toString());
//        Assert.assertEquals(this.projectService.getAllRecords().toString(), domain.getProjects().toString());
//        Assert.assertEquals(this.taskService.getAllRecords().toString(), domain.getTasks().toString());
//    }

}