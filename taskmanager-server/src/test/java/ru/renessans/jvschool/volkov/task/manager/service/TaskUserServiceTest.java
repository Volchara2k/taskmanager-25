package ru.renessans.jvschool.volkov.task.manager.service;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ITaskUserRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.IConfigurationService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEntityManagerFactoryService;
import ru.renessans.jvschool.volkov.task.manager.api.service.ITaskUserService;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataBaseProvider;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataTaskProvider;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataUserProvider;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidDescriptionException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidIdException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidTitleException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidUserException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidUserIdException;
import ru.renessans.jvschool.volkov.task.manager.exception.illegal.IllegalIndexException;
import ru.renessans.jvschool.volkov.task.manager.marker.NegativeImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.ServiceImplementation;
import ru.renessans.jvschool.volkov.task.manager.model.Task;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.repository.TaskUserRepository;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertThrows;

@RunWith(value = JUnitParamsRunner.class)
public final class TaskUserServiceTest {

    @NotNull
    private final IConfigurationService configService = new ConfigurationService();

    @NotNull
    private final IEntityManagerFactoryService entityManagerFactoryService = new EntityManagerFactoryService(configService);

    @NotNull
    private final ITaskUserService taskService = new TaskUserService(entityManagerFactoryService);

//    @Before
//    public void loadConfigurationBefore() {
//        Assert.assertNotNull(this.configService);
//        this.configService.load();
//        this.entityManagerFactoryService.build();
//    }
//
//    @Test
//    @TestCaseName("Run testNegativeAdd for add(\"{0}\", \"{1}\", \"{2}\")")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "invalidOwnerMainFieldsCaseData"
//    )
//    public void testNegativeAdd(
//            @Nullable final String userId,
//            @Nullable final String title,
//            @Nullable final String description
//    ) {
//        Assert.assertNotNull(this.taskService);
//
//        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
//                InvalidUserIdException.class,
//                () -> this.taskService.add(userId, title, description)
//        );
//        Assert.assertNotNull(userIdThrown);
//        Assert.assertNotNull(userIdThrown.getMessage());
//
//        @NotNull final String userIdTemp = UUID.randomUUID().toString();
//        Assert.assertNotNull(userIdTemp);
//        @NotNull final InvalidTitleException titleThrown = assertThrows(
//                InvalidTitleException.class,
//                () -> this.taskService.add(userIdTemp, title, description)
//        );
//        Assert.assertNotNull(titleThrown);
//        Assert.assertNotNull(titleThrown.getMessage());
//
//        @NotNull final String titleTemp = DemoDataConst.TASK_TITLE;
//        @NotNull final InvalidDescriptionException descriptionThrown = assertThrows(
//                InvalidDescriptionException.class,
//                () -> this.taskService.add(userIdTemp, titleTemp, description)
//        );
//        Assert.assertNotNull(descriptionThrown);
//        Assert.assertNotNull(descriptionThrown.getMessage());
//    }
//
//    @Test
//    @TestCaseName("Run testNegativeUpdateByIndex for updateByIndex(\"{0}\", {1}, \"{2}\", \"{3}\")")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "invalidOwnerWithIndexCaseData"
//    )
//    public void testNegativeUpdateByIndex(
//            @Nullable final String userId,
//            @Nullable final Integer index,
//            @Nullable final String title,
//            @Nullable final String description
//    ) {
//        Assert.assertNotNull(this.taskService);
//
//        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
//                InvalidUserIdException.class,
//                () -> this.taskService.updateByIndex(userId, index, title, description)
//        );
//        Assert.assertNotNull(userIdThrown);
//        Assert.assertNotNull(userIdThrown.getMessage());
//
//        @NotNull final String userIdTemp = UUID.randomUUID().toString();
//        Assert.assertNotNull(userIdTemp);
//        @NotNull final IllegalIndexException indexThrown = assertThrows(
//                IllegalIndexException.class,
//                () -> this.taskService.updateByIndex(userIdTemp, index, title, description)
//        );
//        Assert.assertNotNull(indexThrown);
//        Assert.assertNotNull(indexThrown.getMessage());
//
//        @NotNull final Integer indexTemp = 0;
//        Assert.assertNotNull(indexTemp);
//        @NotNull final InvalidTitleException titleThrown = assertThrows(
//                InvalidTitleException.class,
//                () -> this.taskService.updateByIndex(userIdTemp, indexTemp, title, description)
//        );
//        Assert.assertNotNull(titleThrown);
//        Assert.assertNotNull(titleThrown.getMessage());
//
//        @NotNull final String titleTemp = DemoDataConst.TASK_TITLE;
//        @NotNull final InvalidDescriptionException descriptionThrown = assertThrows(
//                InvalidDescriptionException.class,
//                () -> this.taskService.add(userIdTemp, titleTemp, description)
//        );
//        Assert.assertNotNull(descriptionThrown);
//        Assert.assertNotNull(descriptionThrown.getMessage());
//    }
//
//    @Test
//    @TestCaseName("Run testNegativeUpdateById for updateById(\"{0}\", \"{1}\", \"{2}\", \"{3}\")")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "invalidOwnerWithIdCaseData"
//    )
//    public void testNegativeUpdateById(
//            @Nullable final String userId,
//            @Nullable final String id,
//            @Nullable final String title,
//            @Nullable final String description
//    ) {
//        Assert.assertNotNull(this.taskService);
//
//        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
//                InvalidUserIdException.class,
//                () -> this.taskService.updateById(userId, id, title, description)
//        );
//        Assert.assertNotNull(userIdThrown);
//        Assert.assertNotNull(userIdThrown.getMessage());
//
//        @NotNull final String userIdTemp = UUID.randomUUID().toString();
//        Assert.assertNotNull(userIdTemp);
//        @NotNull final InvalidIdException indexThrown = assertThrows(
//                InvalidIdException.class,
//                () -> this.taskService.updateById(userIdTemp, id, title, description)
//        );
//        Assert.assertNotNull(indexThrown);
//        Assert.assertNotNull(indexThrown.getMessage());
//
//        @NotNull final String idTemp = UUID.randomUUID().toString();
//        Assert.assertNotNull(idTemp);
//        @NotNull final InvalidTitleException titleThrown = assertThrows(
//                InvalidTitleException.class,
//                () -> this.taskService.updateById(userIdTemp, idTemp, title, description)
//        );
//        Assert.assertNotNull(titleThrown);
//        Assert.assertNotNull(titleThrown.getMessage());
//
//        @NotNull final String titleTemp = DemoDataConst.TASK_TITLE;
//        @NotNull final InvalidDescriptionException descriptionThrown = assertThrows(
//                InvalidDescriptionException.class,
//                () -> this.taskService.add(userIdTemp, titleTemp, description)
//        );
//        Assert.assertNotNull(descriptionThrown);
//        Assert.assertNotNull(descriptionThrown.getMessage());
//    }
//
//    @Test
//    @TestCaseName("Run testNegativeDeleteByIndex for deleteByIndex(\"{0}\", {1})")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "invalidOwnerIndexCaseData"
//    )
//    public void testNegativeDeleteByIndex(
//            @Nullable final String userId,
//            @Nullable final Integer index
//    ) {
//        Assert.assertNotNull(this.taskService);
//
//        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
//                InvalidUserIdException.class,
//                () -> this.taskService.deleteByIndex(userId, index)
//        );
//        Assert.assertNotNull(userIdThrown);
//        Assert.assertNotNull(userIdThrown.getMessage());
//
//        @NotNull final String userIdTemp = UUID.randomUUID().toString();
//        Assert.assertNotNull(userIdTemp);
//        @NotNull final IllegalIndexException indexThrown = assertThrows(
//                IllegalIndexException.class,
//                () -> this.taskService.deleteByIndex(userIdTemp, index)
//        );
//        Assert.assertNotNull(indexThrown);
//        Assert.assertNotNull(indexThrown.getMessage());
//    }
//
//    @Test
//    @TestCaseName("Run testNegativeDeleteById for deleteById(\"{0}\", \"{1}\")")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "invalidUsersEditableCaseData"
//    )
//    public void testNegativeDeleteById(
//            @Nullable final String userId,
//            @Nullable final String id
//    ) {
//        Assert.assertNotNull(this.taskService);
//
//        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
//                InvalidUserIdException.class,
//                () -> this.taskService.deleteById(userId, id)
//        );
//        Assert.assertNotNull(userIdThrown);
//        Assert.assertNotNull(userIdThrown.getMessage());
//
//        @NotNull final String userIdTemp = UUID.randomUUID().toString();
//        Assert.assertNotNull(userIdTemp);
//        @NotNull final InvalidIdException idThrown = assertThrows(
//                InvalidIdException.class,
//                () -> this.taskService.deleteById(userIdTemp, id)
//        );
//        Assert.assertNotNull(idThrown);
//        Assert.assertNotNull(idThrown.getMessage());
//    }
//
//    @Test
//    @TestCaseName("Run testNegativeDeleteByTitle for deleteByTitle(\"{0}\", \"{1}\")")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "invalidUsersEditableCaseData"
//    )
//    public void testNegativeDeleteByTitle(
//            @Nullable final String userId,
//            @Nullable final String id
//    ) {
//        Assert.assertNotNull(this.taskService);
//
//        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
//                InvalidUserIdException.class,
//                () -> this.taskService.deleteById(userId, id)
//        );
//        Assert.assertNotNull(userIdThrown);
//        Assert.assertNotNull(userIdThrown.getMessage());
//
//        @NotNull final String userIdTemp = UUID.randomUUID().toString();
//        Assert.assertNotNull(userIdTemp);
//        @NotNull final InvalidTitleException titleThrown = assertThrows(
//                InvalidTitleException.class,
//                () -> this.taskService.deleteByTitle(userIdTemp, id)
//        );
//        Assert.assertNotNull(titleThrown);
//        Assert.assertNotNull(titleThrown.getMessage());
//    }
//
//    @Test
//    @TestCaseName("Run testNegativeDeleteAll for deleteAll(\"{0}\")")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataBaseProvider.class,
//            method = "invalidLinesCaseData"
//    )
//    public void testNegativeDeleteAll(
//            @Nullable final String userId
//    ) {
//        Assert.assertNotNull(this.taskService);
//
//        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
//                InvalidUserIdException.class,
//                () -> this.taskService.deleteAll(userId)
//        );
//        Assert.assertNotNull(userIdThrown);
//        Assert.assertNotNull(userIdThrown.getMessage());
//    }
//
//    @Test
//    @TestCaseName("Run testNegativeGetByIndex for getByIndex(\"{0}\", {1})")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "invalidOwnerIndexCaseData"
//    )
//    public void testNegativeGetByIndex(
//            @Nullable final String userId,
//            @Nullable final Integer index
//    ) {
//        Assert.assertNotNull(this.taskService);
//        Assert.assertNotNull(this.taskService);
//
//        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
//                InvalidUserIdException.class,
//                () -> this.taskService.getByIndex(userId, index)
//        );
//        Assert.assertNotNull(userIdThrown);
//        Assert.assertNotNull(userIdThrown.getMessage());
//
//        @NotNull final String userIdTemp = UUID.randomUUID().toString();
//        Assert.assertNotNull(userIdTemp);
//        @NotNull final IllegalIndexException indexThrown = assertThrows(
//                IllegalIndexException.class,
//                () -> this.taskService.getByIndex(userIdTemp, index)
//        );
//        Assert.assertNotNull(indexThrown);
//        Assert.assertNotNull(indexThrown.getMessage());
//    }
//
//    @Test
//    @TestCaseName("Run testNegativeGetById for getById(\"{0}\", \"{1}\")")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "invalidUsersEditableCaseData"
//    )
//    public void testNegativeGetById(
//            @Nullable final String userId,
//            @Nullable final String id
//    ) {
//        Assert.assertNotNull(this.taskService);
//
//        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
//                InvalidUserIdException.class,
//                () -> this.taskService.getById(userId, id)
//        );
//        Assert.assertNotNull(userIdThrown);
//        Assert.assertNotNull(userIdThrown.getMessage());
//
//        @NotNull final String userIdTemp = UUID.randomUUID().toString();
//        Assert.assertNotNull(userIdTemp);
//        @NotNull final InvalidIdException idThrown = assertThrows(
//                InvalidIdException.class,
//                () -> this.taskService.getById(userIdTemp, id)
//        );
//        Assert.assertNotNull(idThrown);
//        Assert.assertNotNull(idThrown.getMessage());
//    }
//
//    @Test
//    @TestCaseName("Run testNegativeGetByTitle for getByTitle(\"{0}\", \"{1}\")")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "invalidUsersEditableCaseData"
//    )
//    public void testNegativeGetByTitle(
//            @Nullable final String userId,
//            @Nullable final String title
//    ) {
//        Assert.assertNotNull(this.taskService);
//
//        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
//                InvalidUserIdException.class,
//                () -> this.taskService.getByTitle(userId, title)
//        );
//        Assert.assertNotNull(userIdThrown);
//        Assert.assertNotNull(userIdThrown.getMessage());
//
//        @NotNull final String userIdTemp = UUID.randomUUID().toString();
//        Assert.assertNotNull(userIdTemp);
//        @NotNull final InvalidTitleException titleThrown = assertThrows(
//                InvalidTitleException.class,
//                () -> this.taskService.getByTitle(userIdTemp, title)
//        );
//        Assert.assertNotNull(titleThrown);
//        Assert.assertNotNull(titleThrown.getMessage());
//    }
//
//    @Test(expected = InvalidUserIdException.class)
//    @TestCaseName("Run testNegativeGetAll for getAll(\"{0}\")")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataBaseProvider.class,
//            method = "invalidLinesCaseData"
//    )
//    public void testNegativeGetAll(
//            @Nullable final String userId
//    ) {
//        Assert.assertNotNull(this.taskService);
//        this.taskService.getAll(userId);
//    }
//
//    @Test(expected = InvalidUserException.class)
//    @TestCaseName("Run testInitialDemoData for initialDemoData({0})")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "invalidCollectionsUsersCaseData"
//    )
//    public void testNegativeInitialDemoData(
//            @Nullable final Collection<User> users
//    ) {
//        Assert.assertNotNull(this.taskService);
//        this.taskService.initialDemoData(users);
//    }
//
//    @Test
//    @TestCaseName("Run testAdd for add({0}, {1})")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataTaskProvider.class,
//            method = "validTasksCaseData"
//    )
//    public void testAdd(
//            @NotNull final User user,
//            @NotNull final Task task
//    ) {
//        Assert.assertNotNull(this.taskService);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(task);
//
//        @NotNull final Task addTask = this.taskService.add(user.getId(), task.getTitle(), task.getDescription());
//        Assert.assertNotNull(addTask);
//        Assert.assertEquals(task.getUserId(), addTask.getUserId());
//        Assert.assertEquals(task.getTitle(), addTask.getTitle());
//        Assert.assertEquals(task.getDescription(), addTask.getDescription());
//    }
//
//    @Test
//    @TestCaseName("Run testUpdateByIndex for updateByIndex({0}, 0, {1})")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataTaskProvider.class,
//            method = "validTasksCaseData"
//    )
//    public void testUpdateByIndex(
//            @NotNull final User user,
//            @NotNull final Task task
//    ) {
//        Assert.assertNotNull(this.taskService);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(task);
//        @NotNull final EntityManager entityManager = this.entityManagerFactoryService.getEntityManager();
//        Assert.assertNotNull(entityManager);
//        @NotNull final ITaskUserRepository taskUserRepository = new TaskUserRepository(entityManager);
//        Assert.assertNotNull(taskUserRepository);
//        @NotNull final Task addRecord = taskUserRepository.persist(task);
//        Assert.assertNotNull(addRecord);
//
//        @NotNull final String newDescription = UUID.randomUUID().toString();
//        Assert.assertNotNull(newDescription);
//        task.setTitle(newDescription);
//        @Nullable final Task updateTask =
//                this.taskService.updateByIndex(user.getId(), 0, task.getTitle(), newDescription);
//        Assert.assertNotNull(updateTask);
//        Assert.assertEquals(addRecord.getId(), updateTask.getId());
//        Assert.assertEquals(addRecord.getUserId(), updateTask.getUserId());
//        Assert.assertEquals(addRecord.getTitle(), updateTask.getTitle());
//        Assert.assertEquals(addRecord.getDescription(), updateTask.getDescription());
//    }
//
//    @Test
//    @TestCaseName("Run testUpdateById for updateById({0}, {1}")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataTaskProvider.class,
//            method = "validTasksCaseData"
//    )
//    public void testUpdateById(
//            @NotNull final User user,
//            @NotNull final Task task
//    ) {
//        Assert.assertNotNull(this.taskService);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(task);
//        @NotNull final EntityManager entityManager = this.entityManagerFactoryService.getEntityManager();
//        Assert.assertNotNull(entityManager);
//        @NotNull final ITaskUserRepository taskUserRepository = new TaskUserRepository(entityManager);
//        Assert.assertNotNull(taskUserRepository);
//        @NotNull final Task addRecord = taskUserRepository.persist(task);
//        Assert.assertNotNull(addRecord);
//
//        @NotNull final String newTitle = UUID.randomUUID().toString();
//        Assert.assertNotNull(newTitle);
//        @Nullable final Task updateTask =
//                this.taskService.updateById(user.getId(), task.getId(), newTitle, task.getDescription());
//        Assert.assertNotNull(updateTask);
//        Assert.assertEquals(addRecord.getId(), updateTask.getId());
//        Assert.assertEquals(addRecord.getUserId(), updateTask.getUserId());
//        Assert.assertEquals(addRecord.getTitle(), updateTask.getTitle());
//        Assert.assertEquals(addRecord.getDescription(), updateTask.getDescription());
//    }
//
//    @Test
//    @TestCaseName("Run testDeleteByIndex for deleteByIndex({0}, 0)")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataTaskProvider.class,
//            method = "validTasksCaseData"
//    )
//    public void testDeleteByIndex(
//            @NotNull final User user,
//            @NotNull final Task task
//    ) {
//        Assert.assertNotNull(this.taskService);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(task);
//        @NotNull final EntityManager entityManager = this.entityManagerFactoryService.getEntityManager();
//        Assert.assertNotNull(entityManager);
//        @NotNull final ITaskUserRepository taskUserRepository = new TaskUserRepository(entityManager);
//        Assert.assertNotNull(taskUserRepository);
//        @NotNull final Task addRecord = taskUserRepository.persist(task);
//        Assert.assertNotNull(addRecord);
//
//        @Nullable final Task deleteTask = this.taskService.deleteByIndex(user.getId(), 0);
//        Assert.assertNotNull(deleteTask);
//        Assert.assertEquals(addRecord.getId(), deleteTask.getId());
//        Assert.assertEquals(addRecord.getUserId(), deleteTask.getUserId());
//        Assert.assertEquals(addRecord.getTitle(), deleteTask.getTitle());
//        Assert.assertEquals(addRecord.getDescription(), deleteTask.getDescription());
//    }
//
//    @Test
//    @TestCaseName("Run testDeleteById for deleteById({0}, {1})")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataTaskProvider.class,
//            method = "validTasksCaseData"
//    )
//    public void testDeleteById(
//            @NotNull final User user,
//            @NotNull final Task task
//    ) {
//        Assert.assertNotNull(this.taskService);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(task);
//        @NotNull final EntityManager entityManager = this.entityManagerFactoryService.getEntityManager();
//        Assert.assertNotNull(entityManager);
//        @NotNull final ITaskUserRepository taskUserRepository = new TaskUserRepository(entityManager);
//        Assert.assertNotNull(taskUserRepository);
//        @NotNull final Task addRecord = taskUserRepository.persist(task);
//        Assert.assertNotNull(addRecord);
//
//        @Nullable final Task deleteTask = this.taskService.deleteById(user.getId(), task.getId());
//        Assert.assertNotNull(deleteTask);
//        Assert.assertEquals(addRecord.getId(), deleteTask.getId());
//        Assert.assertEquals(addRecord.getUserId(), deleteTask.getUserId());
//        Assert.assertEquals(addRecord.getTitle(), deleteTask.getTitle());
//        Assert.assertEquals(addRecord.getDescription(), deleteTask.getDescription());
//    }
//
//    @Test
//    @TestCaseName("Run testDeleteByTitle for deleteByTitle({0}, {1})")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataTaskProvider.class,
//            method = "validTasksCaseData"
//    )
//    public void testDeleteByTitle(
//            @NotNull final User user,
//            @NotNull final Task task
//    ) {
//        Assert.assertNotNull(this.taskService);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(task);
//        @NotNull final EntityManager entityManager = this.entityManagerFactoryService.getEntityManager();
//        Assert.assertNotNull(entityManager);
//        @NotNull final ITaskUserRepository taskUserRepository = new TaskUserRepository(entityManager);
//        Assert.assertNotNull(taskUserRepository);
//        @NotNull final Task addRecord = taskUserRepository.persist(task);
//        Assert.assertNotNull(addRecord);
//
//        @Nullable final Task deleteTask = this.taskService.deleteByTitle(user.getId(), task.getTitle());
//        Assert.assertNotNull(deleteTask);
//        Assert.assertEquals(addRecord.getId(), deleteTask.getId());
//        Assert.assertEquals(addRecord.getUserId(), deleteTask.getUserId());
//        Assert.assertEquals(addRecord.getTitle(), deleteTask.getTitle());
//        Assert.assertEquals(addRecord.getDescription(), deleteTask.getDescription());
//    }
//
//    @Test
//    @TestCaseName("Run testDeleteAll for deleteAll({0})")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataTaskProvider.class,
//            method = "validTasksCaseData"
//    )
//    public void testDeleteAll(
//            @NotNull final User user,
//            @NotNull final Task task
//    ) {
//        Assert.assertNotNull(this.taskService);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(task);
//        @NotNull final EntityManager entityManager = this.entityManagerFactoryService.getEntityManager();
//        Assert.assertNotNull(entityManager);
//        @NotNull final ITaskUserRepository taskUserRepository = new TaskUserRepository(entityManager);
//        Assert.assertNotNull(taskUserRepository);
//        @NotNull final Task addRecord = taskUserRepository.persist(task);
//        Assert.assertNotNull(addRecord);
//
//        @Nullable final Collection<Task> deleteTasks = this.taskService.deleteAll(user.getId());
//        Assert.assertNotNull(deleteTasks);
//        Assert.assertNotEquals(0, deleteTasks.size());
//        final boolean isUserTasks = deleteTasks.stream().allMatch(entity -> user.getId().equals(entity.getUserId()));
//        Assert.assertTrue(isUserTasks);
//    }
//
//    @Test
//    @TestCaseName("Run testGetByIndex for getByIndex({0}, 0)")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataTaskProvider.class,
//            method = "validTasksCaseData"
//    )
//    public void testGetByIndex(
//            @NotNull final User user,
//            @NotNull final Task task
//    ) {
//        Assert.assertNotNull(this.taskService);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(task);
//        @NotNull final EntityManager entityManager = this.entityManagerFactoryService.getEntityManager();
//        Assert.assertNotNull(entityManager);
//        @NotNull final ITaskUserRepository taskUserRepository = new TaskUserRepository(entityManager);
//        Assert.assertNotNull(taskUserRepository);
//        @NotNull final Task addRecord = taskUserRepository.persist(task);
//        Assert.assertNotNull(addRecord);
//
//        @Nullable final Task getTask = this.taskService.getByIndex(user.getId(), 0);
//        Assert.assertNotNull(getTask);
//        Assert.assertEquals(addRecord.getId(), getTask.getId());
//        Assert.assertEquals(addRecord.getUserId(), getTask.getUserId());
//        Assert.assertEquals(addRecord.getTitle(), getTask.getTitle());
//        Assert.assertEquals(addRecord.getDescription(), getTask.getDescription());
//    }
//
//    @Test
//    @TestCaseName("Run testGetById for getById({0}, {1})")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataTaskProvider.class,
//            method = "validTasksCaseData"
//    )
//    public void testGetById(
//            @NotNull final User user,
//            @NotNull final Task task
//    ) {
//        Assert.assertNotNull(this.taskService);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(task);
//        @NotNull final EntityManager entityManager = this.entityManagerFactoryService.getEntityManager();
//        Assert.assertNotNull(entityManager);
//        @NotNull final ITaskUserRepository taskUserRepository = new TaskUserRepository(entityManager);
//        Assert.assertNotNull(taskUserRepository);
//        @NotNull final Task addRecord = taskUserRepository.persist(task);
//        Assert.assertNotNull(addRecord);
//
//        @Nullable final Task getTask = this.taskService.getById(user.getId(), task.getId());
//        Assert.assertNotNull(getTask);
//        Assert.assertEquals(addRecord.getId(), getTask.getId());
//        Assert.assertEquals(addRecord.getUserId(), getTask.getUserId());
//        Assert.assertEquals(addRecord.getTitle(), getTask.getTitle());
//        Assert.assertEquals(addRecord.getDescription(), getTask.getDescription());
//    }
//
//    @Test
//    @TestCaseName("Run testGetByTitle for getByTitle({0}, {1})")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataTaskProvider.class,
//            method = "validTasksCaseData"
//    )
//    public void testGetByTitle(
//            @NotNull final User user,
//            @NotNull final Task task
//    ) {
//        Assert.assertNotNull(this.taskService);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(task);
//        @NotNull final EntityManager entityManager = this.entityManagerFactoryService.getEntityManager();
//        Assert.assertNotNull(entityManager);
//        @NotNull final ITaskUserRepository taskUserRepository = new TaskUserRepository(entityManager);
//        Assert.assertNotNull(taskUserRepository);
//        @NotNull final Task addRecord = taskUserRepository.persist(task);
//        Assert.assertNotNull(addRecord);
//
//        @Nullable final Task getTask = this.taskService.getByTitle(user.getId(), task.getTitle());
//        Assert.assertNotNull(getTask);
//        Assert.assertEquals(addRecord.getId(), getTask.getId());
//        Assert.assertEquals(addRecord.getUserId(), getTask.getUserId());
//        Assert.assertEquals(addRecord.getTitle(), getTask.getTitle());
//        Assert.assertEquals(addRecord.getDescription(), getTask.getDescription());
//    }
//
//    @Test
//    @TestCaseName("Run testGetAll for getAll({0})")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataTaskProvider.class,
//            method = "validTasksCaseData"
//    )
//    public void testGetAll(
//            @NotNull final User user,
//            @NotNull final Task task
//    ) {
//        Assert.assertNotNull(this.taskService);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(task);
//        @NotNull final EntityManager entityManager = this.entityManagerFactoryService.getEntityManager();
//        Assert.assertNotNull(entityManager);
//        @NotNull final ITaskUserRepository taskUserRepository = new TaskUserRepository(entityManager);
//        Assert.assertNotNull(taskUserRepository);
//        @NotNull final Task addRecord = taskUserRepository.persist(task);
//        Assert.assertNotNull(addRecord);
//
//        @Nullable final Collection<Task> getTasks = this.taskService.getAll(user.getId());
//        Assert.assertNotNull(getTasks);
//        Assert.assertNotEquals(0, getTasks.size());
//        final boolean isUserTasks = getTasks.stream().allMatch(entity -> user.getId().equals(entity.getUserId()));
//        Assert.assertTrue(isUserTasks);
//    }
//
//    @Test
//    @TestCaseName("Run testInitialDemoData for initialDemoData()")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validCollectionUsersCaseData"
//    )
//    public void testInitialDemoData(
//            @NotNull final Collection<User> users
//    ) {
//        Assert.assertNotNull(this.taskService);
//        Assert.assertNotNull(users);
//        @NotNull final Collection<Task> initTasks = this.taskService.initialDemoData(users);
//        Assert.assertNotNull(initTasks);
//        Assert.assertNotEquals(0, initTasks.size());
//    }

}