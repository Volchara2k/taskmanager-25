package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.NotNull;

public interface IConfigurationService {

    void load();

    @NotNull
    String getServerHost();

    @NotNull
    Integer getServerPort();

    @NotNull
    String getSessionSalt();

    @NotNull
    Integer getSessionCycle();

    @NotNull
    String getBinPathname();

    @NotNull
    String getBase64Pathname();

    @NotNull
    String getJsonPathname();

    @NotNull
    String getXmlPathname();

    @NotNull
    String getYamlPathname();

    @NotNull
    String getDriverJDBC();

    @NotNull
    String getUrlJDBC();

    @NotNull
    String getDatabaseLogin();

    @NotNull
    String getDatabasePassword();

}