package ru.renessans.jvschool.volkov.task.manager.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.UserDTO;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRole;

import java.util.Collection;

public interface IAdminEndpoint {

    boolean closeAllSessions(
            SessionDTO sessionDTO
    );

    @NotNull
    Collection<SessionDTO> getAllSessions(
            @Nullable SessionDTO sessionDTO
    );

    @Nullable
    UserDTO signUpUserWithUserRole(
            @Nullable SessionDTO sessionDTO,
            @Nullable String login,
            @Nullable String password,
            @Nullable UserRole role
    );

    @Nullable
    UserDTO deleteUserById(
            @Nullable SessionDTO sessionDTO,
            @Nullable String id
    );

    @Nullable
    UserDTO deleteUserByLogin(
            @Nullable SessionDTO sessionDTO,
            @Nullable String login
    );

    boolean deleteAllUsers(
            @Nullable SessionDTO sessionDTO
    );

    @Nullable
    UserDTO lockUserByLogin(
            @Nullable SessionDTO sessionDTO,
            @Nullable String login
    );

    @Nullable
    UserDTO unlockUserByLogin(
            @Nullable SessionDTO sessionDTO,
            @Nullable String login
    );

    @Nullable
    Collection<UserDTO> setAllUsers(
            @Nullable SessionDTO sessionDTO,
            @Nullable Collection<UserDTO> usersDTO
    );

    @NotNull
    Collection<UserDTO> getAllUsers(
            @Nullable SessionDTO sessionDTO
    );

    @NotNull
    Collection<TaskDTO> getAllUsersTasks(
            @Nullable SessionDTO sessionDTO
    );

    @Nullable
    Collection<TaskDTO> setAllUsersTasks(
            @Nullable SessionDTO sessionDTO,
            @Nullable Collection<TaskDTO> tasksDTO
    );

    @NotNull
    Collection<ProjectDTO> getAllUsersProjects(
            @Nullable SessionDTO sessionDTO
    );

    @NotNull
    Collection<ProjectDTO> setAllUsersProjects(
            @Nullable SessionDTO sessionDTO,
            @Nullable Collection<ProjectDTO> projectsDTO
    );

    @Nullable
    UserDTO getUserById(
            @Nullable SessionDTO sessionDTO,
            @Nullable String id
    );

    @Nullable
    UserDTO getUserByLogin(
            @Nullable SessionDTO sessionDTO,
            @Nullable String login
    );

    @Nullable
    UserDTO editProfileById(
            @Nullable SessionDTO sessionDTO,
            @Nullable String id,
            @Nullable String firstName
    );

    @Nullable
    UserDTO editProfileByIdWithLastName(
            @Nullable SessionDTO sessionDTO,
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName
    );

    @Nullable
    UserDTO updatePasswordById(
            @Nullable SessionDTO sessionDTO,
            @Nullable String id,
            @Nullable String newPassword
    );

}