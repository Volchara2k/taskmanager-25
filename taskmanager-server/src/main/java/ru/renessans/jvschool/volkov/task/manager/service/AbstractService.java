package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.IRepository;
import ru.renessans.jvschool.volkov.task.manager.api.IService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEntityManagerFactoryService;
import ru.renessans.jvschool.volkov.task.manager.exception.illegal.IllegalAddModelException;
import ru.renessans.jvschool.volkov.task.manager.exception.illegal.IllegalDeleteModelException;
import ru.renessans.jvschool.volkov.task.manager.exception.illegal.IllegalSetAllModelException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.service.InvalidValueException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.service.InvalidValuesException;
import ru.renessans.jvschool.volkov.task.manager.model.AbstractModel;
import ru.renessans.jvschool.volkov.task.manager.repository.Repository;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Objects;

@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class AbstractService<E extends AbstractModel> implements IService<E> {

    @NotNull
    private final IEntityManagerFactoryService managerFactoryService;

    @NotNull
    @SneakyThrows
    @Override
    public E persist(@Nullable final E value) {
        if (Objects.isNull(value)) throw new InvalidValueException();

        @NotNull final EntityManager entityManager = this.managerFactoryService.getEntityManager();
        @NotNull final IRepository<E> repository = new Repository<>(entityManager);

        @Nullable final E result;
        entityManager.getTransaction().begin();
        try {
            result = repository.persist(value);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception exception) {
            entityManager.getTransaction().rollback();
            throw new IllegalAddModelException(exception.getCause());
        } finally {
            entityManager.close();
        }

        return result;
    }

    @NotNull
    @SneakyThrows
    @Override
    public E merge(@Nullable final E value) {
        if (Objects.isNull(value)) throw new InvalidValueException();

        @NotNull final EntityManager entityManager = this.managerFactoryService.getEntityManager();
        @NotNull final IRepository<E> repository = new Repository<>(entityManager);

        @Nullable final E result;
        entityManager.getTransaction().begin();
        try {
            result = repository.merge(value);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception exception) {
            entityManager.getTransaction().rollback();
            throw new IllegalAddModelException(exception.getCause());
        } finally {
            entityManager.close();
        }

        return result;
    }

    @NotNull
    @SneakyThrows
    @Override
    public Collection<E> setAllRecords(@Nullable final Collection<E> values) {
        if (ValidRuleUtil.isNullOrEmpty(values)) throw new InvalidValuesException();

        @NotNull final EntityManager entityManager = this.managerFactoryService.getEntityManager();
        @NotNull final IRepository<E> repository = new Repository<>(entityManager);

        @Nullable final Collection<E> result;
        entityManager.getTransaction().begin();
        try {
            result = repository.setAllRecords(values);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception exception) {
            entityManager.getTransaction().rollback();
            throw new IllegalSetAllModelException(exception.getCause());
        } finally {
            entityManager.close();
        }

        return result;
    }

    @SneakyThrows
    @Override
    public boolean deletedRecord(@Nullable final E value) {
        if (Objects.isNull(value)) throw new InvalidValueException();

        @NotNull final EntityManager entityManager = this.managerFactoryService.getEntityManager();
        @NotNull final IRepository<E> repository = new Repository<>(entityManager);

        final boolean result;
        entityManager.getTransaction().begin();
        try {
            result = repository.deletedRecord(value);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception exception) {
            entityManager.getTransaction().rollback();
            throw new IllegalDeleteModelException(exception.getCause());
        } finally {
            entityManager.close();
        }

        return result;
    }

    @SneakyThrows
    @Override
    public boolean deletedAllRecords() {
        @NotNull final EntityManager entityManager = this.managerFactoryService.getEntityManager();
        @NotNull final IRepository<E> repository = new Repository<>(entityManager);

        final boolean result;
        entityManager.getTransaction().begin();
        try {
            result = repository.deleteAllRecords();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception exception) {
            entityManager.getTransaction().rollback();
            throw new IllegalDeleteModelException(exception.getCause());
        } finally {
            entityManager.close();
        }

        return result;
    }

}