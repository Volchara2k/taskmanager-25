package ru.renessans.jvschool.volkov.task.manager.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.model.AbstractModel;

import java.util.Collection;

public interface IRepository<E extends AbstractModel> {

    @NotNull
    E persist(@NotNull E value);

    @NotNull
    E merge(@NotNull E value);

    boolean deleteAllRecords();

    @Nullable
    E deleteRecord(@NotNull E value);

    @NotNull
    Collection<E> setAllRecords(@NotNull Collection<E> values);

    boolean deletedRecord(@NotNull E value);

}