package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.Cleanup;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.api.service.IConfigurationService;
import ru.renessans.jvschool.volkov.task.manager.exception.illegal.IllegalConfigurationLoadingException;

import java.io.InputStream;
import java.util.Objects;
import java.util.Properties;

public final class ConfigurationService implements IConfigurationService {

    @NotNull
    private static final String CONFIG_SOURCE = "/configuration.properties";

    @NotNull
    private static final String SERVER_HOST_KEY = "server.host";

    @NotNull
    private static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    private static final String SESSION_CYCLE_KEY = "session.cycle";

    @NotNull
    private static final String SESSION_SALT_KEY = "session.salt";

    @NotNull
    private static final String PATHNAME_BIN = "file.name.bin";

    @NotNull
    private static final String PATHNAME_BASE64 = "file.name.base64";

    @NotNull
    private static final String PATHNAME_JSON = "file.name.json";

    @NotNull
    private static final String PATHNAME_XML = "file.name.xml";

    @NotNull
    private static final String PATHNAME_YAML = "file.name.yaml";

    @NotNull
    private static final String JDBC_DRIVER = "jdbc.driver";

    @NotNull
    private static final String JDBC_URL = "jdbc.url";

    @NotNull
    private static final String DATABASE_LOGIN = "db.login";

    @NotNull
    private static final String DATABASE_PASSWORD = "db.password";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    @Override
    public void load() {
        @NotNull @Cleanup final InputStream inputStream = getClass().getResourceAsStream(CONFIG_SOURCE);
        try {
            this.properties.load(inputStream);
        } catch (@NotNull final Exception exception) {
            throw new IllegalConfigurationLoadingException(exception.getCause());
        }
    }

    @NotNull
    @Override
    public String getServerHost() {
        @NotNull final String propertyHost = this.properties.getProperty(SERVER_HOST_KEY);
        @NotNull final String environmentHost = System.getProperty(SERVER_HOST_KEY);
        if (!Objects.isNull(environmentHost)) return environmentHost;
        return propertyHost;
    }

    @NotNull
    @Override
    public Integer getServerPort() {
        @NotNull final String propertyPort = this.properties.getProperty(SERVER_PORT_KEY);
        @NotNull final String environmentPort = System.getProperty(SERVER_PORT_KEY);
        if (!Objects.isNull(environmentPort)) return Integer.parseInt(environmentPort);
        return Integer.parseInt(propertyPort);
    }

    @NotNull
    @Override
    public String getSessionSalt() {
        return this.properties.getProperty(SESSION_SALT_KEY);
    }

    @NotNull
    @Override
    public Integer getSessionCycle() {
        @NotNull final String cycle = this.properties.getProperty(SESSION_CYCLE_KEY);
        return Integer.parseInt(cycle);
    }

    @NotNull
    @Override
    public String getBinPathname() {
        return this.properties.getProperty(PATHNAME_BIN);
    }

    @NotNull
    @Override
    public String getBase64Pathname() {
        return this.properties.getProperty(PATHNAME_BASE64);
    }

    @NotNull
    @Override
    public String getJsonPathname() {
        return this.properties.getProperty(PATHNAME_JSON);
    }

    @NotNull
    @Override
    public String getXmlPathname() {
        return this.properties.getProperty(PATHNAME_XML);
    }

    @NotNull
    @Override
    public String getYamlPathname() {
        return this.properties.getProperty(PATHNAME_YAML);
    }

    @NotNull
    @Override
    public String getDriverJDBC() {
        return this.properties.getProperty(JDBC_DRIVER);
    }

    @NotNull
    @Override
    public String getUrlJDBC() {
        return this.properties.getProperty(JDBC_URL);
    }

    @NotNull
    @Override
    public String getDatabaseLogin() {
        return this.properties.getProperty(DATABASE_LOGIN);
    }

    @NotNull
    @Override
    public String getDatabasePassword() {
        return this.properties.getProperty(DATABASE_PASSWORD);
    }

}