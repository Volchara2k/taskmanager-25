package ru.renessans.jvschool.volkov.task.manager.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@ToString
@MappedSuperclass
public abstract class AbstractUserOwner extends AbstractModel {

    @Nullable
    @ManyToOne
    private User user;

    @NotNull
    @Column(nullable = false, columnDefinition = "TINYTEXT")
    private String title = "";

    @NotNull
    @Column(nullable = false, columnDefinition = "TEXT")
    private String description = "";

    @NotNull
    @Column(nullable = false, updatable = false)
    private Date creationDate = new Date(System.currentTimeMillis());

    @Nullable
    @Column(nullable = false)
    private String userId;

}