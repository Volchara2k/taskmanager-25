package ru.renessans.jvschool.volkov.task.manager.exception.invalid.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class InvalidEndpointException extends AbstractException {

    @NotNull
    private static final String EMPTY_ENDPOINT = "Ошибка! Параметр \"endpoint\" является null!\n";

    public InvalidEndpointException() {
        super(EMPTY_ENDPOINT);
    }

}