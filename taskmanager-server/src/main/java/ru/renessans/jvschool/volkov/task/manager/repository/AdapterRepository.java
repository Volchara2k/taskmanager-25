package ru.renessans.jvschool.volkov.task.manager.repository;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IAdapterRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.*;
import ru.renessans.jvschool.volkov.task.manager.service.adapter.*;

public final class AdapterRepository implements IAdapterRepository {

    @NotNull
    private final IProjectAdapterService projectAdapter = new ProjectAdapterService();

    @NotNull
    private final ISessionAdapterService sessionAdapter = new SessionAdapterService();

    @NotNull
    private final ITaskAdapterService taskAdapter = new TaskAdapterService();

    @NotNull
    private final IUserAdapterService userAdapter = new UserAdapterService();

    @NotNull
    public IProjectAdapterService getProjectAdapter() {
        return this.projectAdapter;
    }

    @NotNull
    public ISessionAdapterService getSessionAdapter() {
        return this.sessionAdapter;
    }

    @NotNull
    public ITaskAdapterService getTaskAdapter() {
        return this.taskAdapter;
    }

    @NotNull
    public IUserAdapterService getUserAdapter() {
        return this.userAdapter;
    }

}