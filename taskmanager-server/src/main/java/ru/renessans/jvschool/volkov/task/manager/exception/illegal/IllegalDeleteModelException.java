package ru.renessans.jvschool.volkov.task.manager.exception.illegal;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class IllegalDeleteModelException extends AbstractException {

    @NotNull
    private static final String MODEL_DELETE_ILLEGAL =
            "Ошибка! Выполнение удаления модели из базы данных завершилось нелегальным образом!\n";

    public IllegalDeleteModelException(@NotNull final Throwable cause) {
        super(MODEL_DELETE_ILLEGAL, cause);
    }

}