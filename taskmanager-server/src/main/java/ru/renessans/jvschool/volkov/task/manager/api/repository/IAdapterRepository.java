package ru.renessans.jvschool.volkov.task.manager.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.*;

public interface IAdapterRepository {

    @NotNull
    IProjectAdapterService getProjectAdapter();

    @NotNull
    ISessionAdapterService getSessionAdapter();

    @NotNull
    ITaskAdapterService getTaskAdapter();

    @NotNull
    IUserAdapterService getUserAdapter();

}