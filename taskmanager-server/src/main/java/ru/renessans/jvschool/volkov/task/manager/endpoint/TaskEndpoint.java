package ru.renessans.jvschool.volkov.task.manager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.ITaskEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.IAdapterLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.service.ISessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.ITaskUserService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.ISessionAdapterService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.ITaskAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.model.Session;
import ru.renessans.jvschool.volkov.task.manager.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.Collection;
import java.util.stream.Collectors;

@WebService
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint() {
    }

    public TaskEndpoint(
            @NotNull final IServiceLocatorService serviceLocator
    ) {
        super(serviceLocator);
    }

    @WebMethod
    @WebResult(name = "task", partName = "task")
    @Nullable
    @Override
    public TaskDTO addTaskForProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "projectTitle", partName = "projectTitle") @Nullable final String projectTitle,
            @WebParam(name = "title", partName = "title") @Nullable final String title,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        @NotNull final ITaskUserService taskService = super.serviceLocator.getTaskService();
        @NotNull final Task task = taskService.add(current.getUserId(), projectTitle, title, description);

        @NotNull final ITaskAdapterService taskAdapter = adapterService.getTaskAdapter();
        @Nullable final TaskDTO taskDTO = taskAdapter.toDTO(task);
        return taskDTO;
    }

    @WebMethod
    @WebResult(name = "task", partName = "task")
    @Nullable
    @Override
    public TaskDTO addTask(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "title", partName = "title") @Nullable final String title,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        @NotNull final ITaskUserService taskService = super.serviceLocator.getTaskService();
        @NotNull final Task task = taskService.add(current.getUserId(), title, description);

        @NotNull final ITaskAdapterService taskAdapter = adapterService.getTaskAdapter();
        @Nullable final TaskDTO taskDTO = taskAdapter.toDTO(task);
        return taskDTO;
    }

    @WebMethod
    @WebResult(name = "updatedTask", partName = "updatedTask")
    @Nullable
    @Override
    public TaskDTO updateTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "title", partName = "title") @Nullable final String title,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        @NotNull final ITaskUserService taskService = super.serviceLocator.getTaskService();
        @Nullable final Task task = taskService.updateById(current.getUserId(), id, title, description);

        @NotNull final ITaskAdapterService taskAdapter = adapterService.getTaskAdapter();
        @Nullable final TaskDTO taskDTO = taskAdapter.toDTO(task);
        return taskDTO;
    }

    @WebMethod
    @WebResult(name = "updatedTask", partName = "updatedTask")
    @Nullable
    @Override
    public TaskDTO updateTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index,
            @WebParam(name = "title", partName = "title") @Nullable final String title,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        @NotNull final ITaskUserService taskService = super.serviceLocator.getTaskService();
        @Nullable final Task task = taskService.updateByIndex(current.getUserId(), index, title, description);

        @NotNull final ITaskAdapterService taskAdapter = adapterService.getTaskAdapter();
        @Nullable final TaskDTO taskDTO = taskAdapter.toDTO(task);
        return taskDTO;
    }

    @WebMethod
    @WebResult(name = "deletedTask", partName = "deletedTask")
    @Nullable
    @Override
    public TaskDTO deleteTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        @NotNull final ITaskUserService taskService = super.serviceLocator.getTaskService();
        @Nullable final Task task = taskService.deleteById(current.getUserId(), id);

        @NotNull final ITaskAdapterService taskAdapter = adapterService.getTaskAdapter();
        @Nullable final TaskDTO taskDTO = taskAdapter.toDTO(task);
        return taskDTO;
    }

    @WebMethod
    @WebResult(name = "deletedTask", partName = "deletedTask")
    @Nullable
    @Override
    public TaskDTO deleteTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        @NotNull final ITaskUserService taskService = super.serviceLocator.getTaskService();
        @Nullable final Task task = taskService.deleteByIndex(current.getUserId(), index);

        @NotNull final ITaskAdapterService taskAdapter = adapterService.getTaskAdapter();
        @Nullable final TaskDTO taskDTO = taskAdapter.toDTO(task);
        return taskDTO;
    }

    @WebMethod
    @WebResult(name = "deletedTask", partName = "deletedTask")
    @Nullable
    @Override
    public TaskDTO deleteTaskByTitle(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "title", partName = "title") @Nullable final String title
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        @NotNull final ITaskUserService taskService = super.serviceLocator.getTaskService();
        @Nullable final Task task = taskService.deleteByTitle(current.getUserId(), title);

        @NotNull final ITaskAdapterService taskAdapter = adapterService.getTaskAdapter();
        @Nullable final TaskDTO taskDTO = taskAdapter.toDTO(task);
        return taskDTO;
    }

    @WebMethod
    @WebResult(name = "deletedTasks", partName = "deletedTasks")
    @NotNull
    @Override
    public Collection<TaskDTO> deleteAllTasks(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        @NotNull final ITaskUserService taskService = super.serviceLocator.getTaskService();

        @NotNull final ITaskAdapterService taskAdapter = adapterService.getTaskAdapter();
        return taskService.deleteAll(current.getUserId())
                .stream()
                .map(taskAdapter::toDTO)
                .collect(Collectors.toList());
    }

    @WebMethod
    @WebResult(name = "task", partName = "task")
    @Nullable
    @Override
    public TaskDTO getTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        @NotNull final ITaskUserService taskService = super.serviceLocator.getTaskService();
        @Nullable final Task task = taskService.getById(current.getUserId(), id);

        @NotNull final ITaskAdapterService taskAdapter = adapterService.getTaskAdapter();
        @Nullable final TaskDTO taskDTO = taskAdapter.toDTO(task);
        return taskDTO;
    }

    @WebMethod
    @WebResult(name = "task", partName = "task")
    @Nullable
    @Override
    public TaskDTO getTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        @NotNull final ITaskUserService taskService = super.serviceLocator.getTaskService();
        @Nullable final Task task = taskService.getByIndex(current.getUserId(), index);

        @NotNull final ITaskAdapterService taskAdapter = adapterService.getTaskAdapter();
        @Nullable final TaskDTO taskDTO = taskAdapter.toDTO(task);
        return taskDTO;
    }

    @WebMethod
    @WebResult(name = "task", partName = "task")
    @Nullable
    @Override
    public TaskDTO getTaskByTitle(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "title", partName = "title") @Nullable final String title
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        @NotNull final ITaskUserService taskService = super.serviceLocator.getTaskService();
        @Nullable final Task task = taskService.getByTitle(current.getUserId(), title);

        @NotNull final ITaskAdapterService taskAdapter = adapterService.getTaskAdapter();
        @Nullable final TaskDTO taskDTO = taskAdapter.toDTO(task);
        return taskDTO;
    }

    @WebMethod
    @WebResult(name = "tasks", partName = "tasks")
    @NotNull
    @Override
    public Collection<TaskDTO> getAllTasks(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        @NotNull final ITaskUserService taskService = super.serviceLocator.getTaskService();

        @NotNull final ITaskAdapterService taskAdapter = adapterService.getTaskAdapter();
        return taskService.getAll(current.getUserId())
                .stream()
                .map(taskAdapter::toDTO)
                .collect(Collectors.toList());
    }

}