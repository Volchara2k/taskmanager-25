package ru.renessans.jvschool.volkov.task.manager.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.ILocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.service.*;
import ru.renessans.jvschool.volkov.task.manager.endpoint.*;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.service.LocatorService;
import ru.renessans.jvschool.volkov.task.manager.util.EndpointUtil;

import java.util.Collection;
import java.util.List;

public final class Bootstrap {

    @NotNull final ILocatorService locatorService = new LocatorService();

    @NotNull final IServiceLocatorService serviceLocator = locatorService.getServiceLocator();

    public void run() {
        loadConfiguration();
        buildEntityManagerFactory();
        initialDemoData();
        publishWebServices();
    }

    private void loadConfiguration() {
        @NotNull final IConfigurationService configService = this.serviceLocator.getConfigurationService();
        configService.load();
    }

    private void buildEntityManagerFactory() {
        @NotNull final IEntityManagerFactoryService connectionService = this.serviceLocator.getEntityManagerService();
        connectionService.build();
    }

    private void initialDemoData() {
        @NotNull final IUserService userService = this.serviceLocator.getUserService();
        @NotNull final Collection<User> users = userService.initialDemoData();
        @NotNull final ITaskUserService taskService = this.serviceLocator.getTaskService();
        taskService.initialDemoData(users);
        @NotNull final IProjectUserService projectService = this.serviceLocator.getProjectService();
        projectService.initialDemoData(users);
    }

    private void publishWebServices() {
        @NotNull final IEndpointLocatorService endpointService = this.locatorService.getEndpointLocator();
        @NotNull final List<AbstractEndpoint> endpoints = endpointService.createEndpoints(this.serviceLocator);
        @Nullable final IConfigurationService configService = this.serviceLocator.getConfigurationService();
        @Nullable final String host = configService.getServerHost();
        @Nullable final Integer port = configService.getServerPort();
        EndpointUtil.publish(endpoints, host, port);
    }

}