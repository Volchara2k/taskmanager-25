package ru.renessans.jvschool.volkov.task.manager.service.adapter;

import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.ITaskAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.model.Task;

import java.util.Objects;

public final class TaskAdapterService implements ITaskAdapterService {

    @Nullable
    @Override
    public TaskDTO toDTO(@Nullable final Task convertible) {
        if (Objects.isNull(convertible)) return null;
        return TaskDTO.builder()
                .id(convertible.getId())
                .userId(convertible.getUserId())
                .title(convertible.getTitle())
                .description(convertible.getDescription())
                .creationDate(convertible.getCreationDate())
                .build();
    }

    @Nullable
    @Override
    public Task toModel(@Nullable final TaskDTO convertible) {
        if (Objects.isNull(convertible)) return null;
        return Task.builder()
                .id(convertible.getId())
                .userId(convertible.getUserId())
                .title(convertible.getTitle())
                .description(convertible.getDescription())
                .creationDate(convertible.getCreationDate())
                .build();
    }

}