package ru.renessans.jvschool.volkov.task.manager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.IProjectEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.IAdapterLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IProjectUserService;
import ru.renessans.jvschool.volkov.task.manager.api.service.ISessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.IProjectAdapterService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.ISessionAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.model.Project;
import ru.renessans.jvschool.volkov.task.manager.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.Collection;
import java.util.stream.Collectors;

@WebService
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint() {
    }

    public ProjectEndpoint(
            @NotNull final IServiceLocatorService serviceLocator
    ) {
        super(serviceLocator);
    }

    @WebMethod
    @WebResult(name = "project", partName = "project")
    @Nullable
    @Override
    public ProjectDTO addProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "title", partName = "title") @Nullable final String title,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        @NotNull final IProjectUserService projectService = super.serviceLocator.getProjectService();
        @NotNull final Project project = projectService.add(current.getUserId(), title, description);

        @NotNull final IProjectAdapterService taskAdapter = adapterService.getProjectAdapter();
        @Nullable final ProjectDTO projectDTO = taskAdapter.toDTO(project);
        return projectDTO;
    }

    @WebMethod
    @WebResult(name = "updatedProject", partName = "updatedProject")
    @Nullable
    @Override
    public ProjectDTO updateProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index,
            @WebParam(name = "title", partName = "title") @Nullable final String title,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        @NotNull final IProjectUserService projectService = super.serviceLocator.getProjectService();
        @Nullable final Project project = projectService.updateByIndex(current.getUserId(), index, title, description);

        @NotNull final IProjectAdapterService taskAdapter = adapterService.getProjectAdapter();
        @Nullable final ProjectDTO projectDTO = taskAdapter.toDTO(project);
        return projectDTO;
    }

    @WebMethod
    @WebResult(name = "updatedProject", partName = "updatedProject")
    @Nullable
    @Override
    public ProjectDTO updateProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "title", partName = "title") @Nullable final String title,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        @NotNull final IProjectUserService projectService = super.serviceLocator.getProjectService();
        @Nullable final Project project = projectService.updateById(current.getUserId(), id, title, description);

        @NotNull final IProjectAdapterService taskAdapter = adapterService.getProjectAdapter();
        @Nullable final ProjectDTO projectDTO = taskAdapter.toDTO(project);
        return projectDTO;
    }

    @WebMethod
    @WebResult(name = "deletedProject", partName = "deletedProject")
    @Nullable
    @Override
    public ProjectDTO deleteProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        @NotNull final IProjectUserService projectService = super.serviceLocator.getProjectService();
        @Nullable final Project project = projectService.deleteById(current.getUserId(), id);

        @NotNull final IProjectAdapterService taskAdapter = adapterService.getProjectAdapter();
        @Nullable final ProjectDTO projectDTO = taskAdapter.toDTO(project);
        return projectDTO;
    }

    @WebMethod
    @WebResult(name = "deletedProject", partName = "deletedProject")
    @Nullable
    @Override
    public ProjectDTO deleteProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        @NotNull final IProjectUserService projectService = super.serviceLocator.getProjectService();
        @Nullable final Project project = projectService.deleteByIndex(current.getUserId(), index);

        @NotNull final IProjectAdapterService taskAdapter = adapterService.getProjectAdapter();
        @Nullable final ProjectDTO projectDTO = taskAdapter.toDTO(project);
        return projectDTO;
    }

    @WebMethod
    @WebResult(name = "deletedProject", partName = "deletedProject")
    @Nullable
    @Override
    public ProjectDTO deleteProjectByTitle(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "title", partName = "title") @Nullable final String title
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        @NotNull final IProjectUserService projectService = super.serviceLocator.getProjectService();
        @Nullable final Project project = projectService.deleteByTitle(current.getUserId(), title);

        @NotNull final IProjectAdapterService taskAdapter = adapterService.getProjectAdapter();
        @Nullable final ProjectDTO projectDTO = taskAdapter.toDTO(project);
        return projectDTO;
    }

    @WebMethod
    @WebResult(name = "deletedProjects", partName = "deletedProjects")
    @NotNull
    @Override
    public Collection<ProjectDTO> deleteAllProjects(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        @NotNull final IProjectUserService projectService = super.serviceLocator.getProjectService();

        @NotNull final IProjectAdapterService taskAdapter = adapterService.getProjectAdapter();
        return projectService.deleteAll(current.getUserId())
                .stream()
                .map(taskAdapter::toDTO)
                .collect(Collectors.toList());
    }

    @WebMethod
    @WebResult(name = "project", partName = "project")
    @Nullable
    @Override
    public ProjectDTO getProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        @NotNull final IProjectUserService projectService = super.serviceLocator.getProjectService();
        @Nullable final Project project = projectService.getById(current.getUserId(), id);

        @NotNull final IProjectAdapterService taskAdapter = adapterService.getProjectAdapter();
        @Nullable final ProjectDTO projectDTO = taskAdapter.toDTO(project);
        return projectDTO;
    }

    @WebMethod
    @WebResult(name = "project", partName = "project")
    @Nullable
    @Override
    public ProjectDTO getProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        @NotNull final IProjectUserService projectService = super.serviceLocator.getProjectService();
        @Nullable final Project project = projectService.getByIndex(current.getUserId(), index);

        @NotNull final IProjectAdapterService taskAdapter = adapterService.getProjectAdapter();
        @Nullable final ProjectDTO projectDTO = taskAdapter.toDTO(project);
        return projectDTO;
    }

    @WebMethod
    @WebResult(name = "project", partName = "project")
    @Nullable
    @Override
    public ProjectDTO getProjectByTitle(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "title", partName = "title") @Nullable final String title
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        @NotNull final IProjectUserService projectService = super.serviceLocator.getProjectService();
        @Nullable final Project project = projectService.getByTitle(current.getUserId(), title);

        @NotNull final IProjectAdapterService taskAdapter = adapterService.getProjectAdapter();
        @Nullable final ProjectDTO projectDTO = taskAdapter.toDTO(project);
        return projectDTO;
    }

    @WebMethod
    @WebResult(name = "projects", partName = "projects")
    @NotNull
    @Override
    public Collection<ProjectDTO> getAllProjects(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        @NotNull final IProjectUserService projectService = super.serviceLocator.getProjectService();

        @NotNull final IProjectAdapterService taskAdapter = adapterService.getProjectAdapter();
        return projectService.getAll(current.getUserId())
                .stream()
                .map(taskAdapter::toDTO)
                .collect(Collectors.toList());
    }

}