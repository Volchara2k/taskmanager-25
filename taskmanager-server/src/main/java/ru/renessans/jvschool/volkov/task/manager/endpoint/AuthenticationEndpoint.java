package ru.renessans.jvschool.volkov.task.manager.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.IAuthenticationEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.IAdapterLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IAuthenticationService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.IUserAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.UserDTO;
import ru.renessans.jvschool.volkov.task.manager.enumeration.PermissionValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRole;
import ru.renessans.jvschool.volkov.task.manager.exception.security.AccessFailureException;
import ru.renessans.jvschool.volkov.task.manager.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.Objects;

@WebService
public final class AuthenticationEndpoint extends AbstractEndpoint implements IAuthenticationEndpoint {

    public AuthenticationEndpoint() {
    }

    public AuthenticationEndpoint(
            @NotNull final IServiceLocatorService serviceLocator
    ) {
        super(serviceLocator);
    }

    @WebMethod
    @WebResult(name = "user", partName = "user")
    @Nullable
    @SneakyThrows
    @Override
    public UserDTO signUpUser(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password
    ) {
        @NotNull final IAuthenticationService authService = super.serviceLocator.getAuthenticationService();
        @Nullable String userId = null;
        if (!Objects.isNull(sessionDTO)) userId = sessionDTO.getUserId();
        @NotNull final PermissionValidState permissionValidState = authService.verifyValidPermission(userId, UserRole.UNKNOWN);
        if (permissionValidState.isNotSuccess()) throw new AccessFailureException(permissionValidState.getTitle());

        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final IUserAdapterService userAdapter = adapterService.getUserAdapter();
        @NotNull final User user = authService.signUp(login, password);
        @Nullable final UserDTO userDTO = userAdapter.toDTO(user);
        return userDTO;
    }

    @WebMethod
    @WebResult(name = "user", partName = "user")
    @Nullable
    @SneakyThrows
    @Override
    public UserDTO signUpUserWithFirstName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password,
            @WebParam(name = "firstName", partName = "firstName") @Nullable final String firstName
    ) {
        @NotNull final IAuthenticationService authService = super.serviceLocator.getAuthenticationService();
        @Nullable String userId = null;
        if (!Objects.isNull(sessionDTO)) userId = sessionDTO.getUserId();
        @NotNull final PermissionValidState permissionValidState = authService.verifyValidPermission(userId, UserRole.UNKNOWN);
        if (permissionValidState.isNotSuccess()) throw new AccessFailureException(permissionValidState.getTitle());

        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final IUserAdapterService userAdapter = adapterService.getUserAdapter();
        @NotNull final User user = authService.signUp(login, password, firstName);
        @Nullable final UserDTO userDTO = userAdapter.toDTO(user);
        return userDTO;
    }

    @WebMethod
    @WebResult(name = "user", partName = "user")
    @NotNull
    @Override
    public UserRole getUserRole(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @Nullable final IAuthenticationService authService = super.serviceLocator.getAuthenticationService();
        @Nullable String userId = null;
        if (!Objects.isNull(sessionDTO)) userId = sessionDTO.getUserId();
        return authService.getUserRole(userId);
    }

}