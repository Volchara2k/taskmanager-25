package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IProjectUserRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEntityManagerFactoryService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IProjectUserService;
import ru.renessans.jvschool.volkov.task.manager.exception.illegal.IllegalAddModelException;
import ru.renessans.jvschool.volkov.task.manager.exception.illegal.IllegalDeleteModelException;
import ru.renessans.jvschool.volkov.task.manager.exception.illegal.IllegalIndexException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidDescriptionException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidIdException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidTitleException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidUserIdException;
import ru.renessans.jvschool.volkov.task.manager.model.Project;
import ru.renessans.jvschool.volkov.task.manager.repository.ProjectUserRepository;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import javax.persistence.EntityManager;
import java.util.Collection;

public final class ProjectUserService extends AbstractUserOwnerService<Project> implements IProjectUserService {

    @NotNull
    private final IEntityManagerFactoryService managerFactoryService;

    public ProjectUserService(
            @NotNull final IEntityManagerFactoryService managerFactoryService
    ) {
        super(managerFactoryService);
        this.managerFactoryService = managerFactoryService;
    }

    @NotNull
    @SneakyThrows
    @Override
    public Project add(
            @Nullable final String userId,
            @Nullable final String title,
            @Nullable final String description
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new InvalidTitleException();
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new InvalidDescriptionException();
        @NotNull final Project project = new Project(title, description, userId);

        @NotNull final EntityManager entityManager = this.managerFactoryService.getEntityManager();
        @NotNull final IProjectUserRepository projectRepository = new ProjectUserRepository(entityManager);

        @Nullable Project result;
        entityManager.getTransaction().begin();
        try {
            result = projectRepository.persist(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception exception) {
            entityManager.getTransaction().rollback();
            throw new IllegalAddModelException(exception.getCause());
        } finally {
            entityManager.close();
        }

        return result;
    }

    @NotNull
    @SneakyThrows
    @Override
    public Project deleteByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IllegalIndexException();

        @NotNull final EntityManager entityManager = this.managerFactoryService.getEntityManager();
        @NotNull final IProjectUserRepository projectRepository = new ProjectUserRepository(entityManager);

        @Nullable Project result;
        entityManager.getTransaction().begin();
        try {
            result = projectRepository.deleteByIndex(userId, index);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception exception) {
            entityManager.getTransaction().rollback();
            throw new IllegalDeleteModelException(exception.getCause());
        } finally {
            entityManager.close();
        }

        return result;
    }

    @NotNull
    @SneakyThrows
    @Override
    public Project deleteById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidIdException();

        @NotNull final EntityManager entityManager = this.managerFactoryService.getEntityManager();
        @NotNull final IProjectUserRepository projectRepository = new ProjectUserRepository(entityManager);

        @Nullable Project result;
        entityManager.getTransaction().begin();
        try {
            result = projectRepository.deleteById(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception exception) {
            entityManager.getTransaction().rollback();
            throw new IllegalDeleteModelException(exception.getCause());
        } finally {
            entityManager.close();
        }

        return result;
    }

    @NotNull
    @SneakyThrows
    @Override
    public Project deleteByTitle(
            @Nullable final String userId,
            @Nullable final String title
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new InvalidTitleException();

        @NotNull final EntityManager entityManager = this.managerFactoryService.getEntityManager();
        @NotNull final IProjectUserRepository projectRepository = new ProjectUserRepository(entityManager);

        @Nullable Project result;
        entityManager.getTransaction().begin();
        try {
            result = projectRepository.deleteByTitle(userId, title);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception exception) {
            entityManager.getTransaction().rollback();
            throw new IllegalDeleteModelException(exception.getCause());
        } finally {
            entityManager.close();
        }

        return result;
    }

    @NotNull
    @SneakyThrows
    @Override
    public Collection<Project> deleteAll(
            @Nullable final String userId
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();

        @NotNull final EntityManager entityManager = this.managerFactoryService.getEntityManager();
        @NotNull final IProjectUserRepository projectRepository = new ProjectUserRepository(entityManager);

        @Nullable Collection<Project> result;
        entityManager.getTransaction().begin();
        try {
            result = projectRepository.deleteAll(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception exception) {
            entityManager.getTransaction().rollback();
            throw new IllegalDeleteModelException(exception.getCause());
        } finally {
            entityManager.close();
        }

        return result;
    }

    @Nullable
    @SneakyThrows
    @Override
    public Project getByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IllegalIndexException();
        @NotNull final EntityManager entityManager = this.managerFactoryService.getEntityManager();
        @NotNull final IProjectUserRepository projectRepository = new ProjectUserRepository(entityManager);
        return projectRepository.getByIndex(userId, index);
    }

    @Nullable
    @SneakyThrows
    @Override
    public Project getById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidIdException();
        @NotNull final EntityManager entityManager = this.managerFactoryService.getEntityManager();
        @NotNull final IProjectUserRepository projectRepository = new ProjectUserRepository(entityManager);
        return projectRepository.getById(userId, id);
    }

    @Nullable
    @SneakyThrows
    @Override
    public Project getByTitle(
            @Nullable final String userId,
            @Nullable final String title
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new InvalidTitleException();
        @NotNull final EntityManager entityManager = this.managerFactoryService.getEntityManager();
        @NotNull final IProjectUserRepository projectRepository = new ProjectUserRepository(entityManager);
        return projectRepository.getByTitle(userId, title);
    }

    @NotNull
    @SneakyThrows
    @Override
    public Collection<Project> getAll(
            @Nullable final String userId
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        @NotNull final EntityManager entityManager = this.managerFactoryService.getEntityManager();
        @NotNull final IProjectUserRepository projectRepository = new ProjectUserRepository(entityManager);
        return projectRepository.getAll(userId);
    }

    @NotNull
    @Override
    public Collection<Project> getAllRecords() {
        @NotNull final EntityManager entityManager = this.managerFactoryService.getEntityManager();
        @NotNull final IProjectUserRepository projectRepository = new ProjectUserRepository(entityManager);
        return projectRepository.getAllRecords();
    }

}