package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEntityManagerFactoryService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IOwnerUserService;
import ru.renessans.jvschool.volkov.task.manager.exception.illegal.IllegalIndexException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidDescriptionException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidIdException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidTaskException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidTitleException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidUserException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidUserIdException;
import ru.renessans.jvschool.volkov.task.manager.model.AbstractUserOwner;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import java.util.*;

public abstract class AbstractUserOwnerService<E extends AbstractUserOwner> extends AbstractService<E> implements IOwnerUserService<E> {

    public AbstractUserOwnerService(
            @NotNull final IEntityManagerFactoryService managerFactoryService
    ) {
        super(managerFactoryService);
    }

    @NotNull
    @SneakyThrows
    @Override
    public E updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String title,
            @Nullable final String description
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IllegalIndexException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new InvalidTitleException();
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new InvalidDescriptionException();

        @Nullable final E value = getByIndex(userId, index);
        if (Objects.isNull(value)) throw new InvalidTaskException();
        value.setTitle(title);
        value.setDescription(description);

        return super.merge(value);
    }

    @NotNull
    @SneakyThrows
    @Override
    public E updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String title,
            @Nullable final String description
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidIdException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new InvalidTitleException();
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new InvalidDescriptionException();

        @Nullable final E value = getById(userId, id);
        if (Objects.isNull(value)) throw new InvalidTaskException();
        value.setTitle(title);
        value.setDescription(description);

        return super.merge(value);
    }

    @NotNull
    @SneakyThrows
    @Override
    public Collection<E> initialDemoData(
            @Nullable final Collection<User> users
    ) {
        if (ValidRuleUtil.isNullOrEmpty(users)) throw new InvalidUserException();

        @NotNull final List<E> demoData = new ArrayList<>();
        users.forEach(user -> {
            deleteAll(user.getId());
            @NotNull final E task = add(user.getId(), UUID.randomUUID().toString(), UUID.randomUUID().toString());
            demoData.add(task);
        });

        return demoData;
    }

}