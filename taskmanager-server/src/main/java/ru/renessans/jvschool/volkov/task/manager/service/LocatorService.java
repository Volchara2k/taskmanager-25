package ru.renessans.jvschool.volkov.task.manager.service;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.api.ILocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.repository.EndpointRepository;
import ru.renessans.jvschool.volkov.task.manager.repository.ServiceLocatorRepository;


public final class LocatorService implements ILocatorService {

    @NotNull
    private final IServiceLocatorRepository serviceLocatorRepository = new ServiceLocatorRepository();

    @NotNull
    private final IServiceLocatorService serviceLocator = new ServiceLocatorService(serviceLocatorRepository);

    @NotNull
    private final IEndpointLocatorRepository endpointRepository = new EndpointRepository();

    @NotNull
    private final EndpointService endpointService = new EndpointService(endpointRepository);

    @NotNull
    @Override
    public IEndpointLocatorService getEndpointLocator() {
        return this.endpointService;
    }

    @NotNull
    @Override
    public IServiceLocatorService getServiceLocator() {
        return this.serviceLocator;
    }

}