package ru.renessans.jvschool.volkov.task.manager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.IAdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.IAdapterLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IDataInterChangeService;
import ru.renessans.jvschool.volkov.task.manager.api.service.ISessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.ISessionAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.DomainDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRole;
import ru.renessans.jvschool.volkov.task.manager.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

@WebService
public final class AdminDataInterChangeEndpoint extends AbstractEndpoint implements IAdminDataInterChangeEndpoint {

    @NotNull
    private static final UserRole ROLE = UserRole.ADMIN;

    public AdminDataInterChangeEndpoint() {
    }

    public AdminDataInterChangeEndpoint(
            @NotNull final IServiceLocatorService serviceLocator
    ) {
        super(serviceLocator);
    }

    @WebMethod
    @WebResult(name = "isClearedBinData", partName = "isClearedBinData")
    @Override
    public boolean dataBinClear(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        sessionService.validateSession(current, ROLE);

        @NotNull final IDataInterChangeService interChangeService = super.serviceLocator.getDataInterChangeService();
        return interChangeService.dataBinClear();
    }

    @WebMethod
    @WebResult(name = "isClearedBase64Data", partName = "isClearedBase64Data")
    @Override
    public boolean dataBase64Clear(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        sessionService.validateSession(current, ROLE);

        @NotNull final IDataInterChangeService interChangeService = super.serviceLocator.getDataInterChangeService();
        return interChangeService.dataBase64Clear();
    }

    @WebMethod
    @WebResult(name = "isClearedJsonData", partName = "isClearedJsonData")
    @Override
    public boolean dataJsonClear(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        sessionService.validateSession(current, ROLE);

        @NotNull final IDataInterChangeService interChangeService = super.serviceLocator.getDataInterChangeService();
        return interChangeService.dataJsonClear();
    }

    @WebMethod
    @WebResult(name = "isClearedXmlData", partName = "isClearedXmlData")
    @Override
    public boolean dataXmlClear(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        sessionService.validateSession(current, ROLE);

        @NotNull final IDataInterChangeService interChangeService = super.serviceLocator.getDataInterChangeService();
        return interChangeService.dataXmlClear();
    }

    @WebMethod
    @WebResult(name = "isClearedYamlData", partName = "isClearedYamlData")
    @Override
    public boolean dataYamlClear(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        sessionService.validateSession(current, ROLE);

        @NotNull final IDataInterChangeService interChangeService = super.serviceLocator.getDataInterChangeService();
        return interChangeService.dataYamlClear();
    }

    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    @Override
    public DomainDTO exportDataBin(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        sessionService.validateSession(current, ROLE);

        @NotNull final IDataInterChangeService interChangeService = super.serviceLocator.getDataInterChangeService();
        return interChangeService.exportDataBin();
    }

    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    @Override
    public DomainDTO exportDataBase64(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        sessionService.validateSession(current, ROLE);

        @NotNull final IDataInterChangeService interChangeService = super.serviceLocator.getDataInterChangeService();
        return interChangeService.exportDataBase64();
    }

    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    @Override
    public DomainDTO exportDataJson(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        sessionService.validateSession(current, ROLE);

        @NotNull final IDataInterChangeService interChangeService = super.serviceLocator.getDataInterChangeService();
        return interChangeService.exportDataJson();
    }

    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    @Override
    public DomainDTO exportDataXml(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        sessionService.validateSession(current, ROLE);

        @NotNull final IDataInterChangeService interChangeService = super.serviceLocator.getDataInterChangeService();
        return interChangeService.exportDataXml();
    }

    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    @Override
    public DomainDTO exportDataYaml(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        sessionService.validateSession(current, ROLE);

        @NotNull final IDataInterChangeService interChangeService = super.serviceLocator.getDataInterChangeService();
        return interChangeService.exportDataYaml();
    }

    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    @Override
    public DomainDTO importDataBin(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        sessionService.validateSession(current, ROLE);

        @NotNull final IDataInterChangeService interChangeService = super.serviceLocator.getDataInterChangeService();
        return interChangeService.importDataBin();
    }

    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    @Override
    public DomainDTO importDataBase64(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        sessionService.validateSession(current, ROLE);

        @NotNull final IDataInterChangeService interChangeService = super.serviceLocator.getDataInterChangeService();
        return interChangeService.importDataBase64();
    }

    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    @Override
    public DomainDTO importDataJson(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        sessionService.validateSession(current, ROLE);

        @NotNull final IDataInterChangeService interChangeService = super.serviceLocator.getDataInterChangeService();
        return interChangeService.importDataJson();
    }

    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    @Override
    public DomainDTO importDataXml(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        sessionService.validateSession(current, ROLE);

        @NotNull final IDataInterChangeService interChangeService = super.serviceLocator.getDataInterChangeService();
        return interChangeService.importDataXml();
    }

    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    @Override
    public DomainDTO importDataYaml(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();

        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        sessionService.validateSession(current, ROLE);

        @NotNull final IDataInterChangeService interChangeService = super.serviceLocator.getDataInterChangeService();
        return interChangeService.importDataYaml();
    }

}