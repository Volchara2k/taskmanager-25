package ru.renessans.jvschool.volkov.task.manager.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.Collection;

@Data
@XmlType
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public final class DomainDTO extends AbstractDTO {

    private static final long serialVersionUID = 1L;

    @NotNull
    private Collection<ProjectDTO> projects = new ArrayList<>();

    @NotNull
    private Collection<TaskDTO> tasks = new ArrayList<>();

    @NotNull
    private Collection<UserDTO> users = new ArrayList<>();

}