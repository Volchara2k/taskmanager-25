package ru.renessans.jvschool.volkov.task.manager.repository;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IAdapterRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.*;
import ru.renessans.jvschool.volkov.task.manager.service.*;
import ru.renessans.jvschool.volkov.task.manager.service.adapter.AdapterLocatorService;

public final class ServiceLocatorRepository implements IServiceLocatorRepository {

    @NotNull
    private final IConfigurationService configService = new ConfigurationService();

    @NotNull
    private final IEntityManagerFactoryService managerFactoryService = new EntityManagerFactoryService(configService);

    @NotNull
    private final IAdapterRepository adapterRepository = new AdapterRepository();

    @NotNull
    private final IAdapterLocatorService adapterService = new AdapterLocatorService(adapterRepository);


    @NotNull
    private final ITaskUserService taskService = new TaskUserService(managerFactoryService);

    @NotNull
    private final IProjectUserService projectService = new ProjectUserService(managerFactoryService);

    @NotNull
    private final IUserService userService = new UserService(managerFactoryService);

    @NotNull
    private final IAuthenticationService authService = new AuthenticationService(userService);

    @NotNull
    private final ISessionService sessionService = new SessionService(
            managerFactoryService, authService, userService, configService
    );


    @NotNull
    private final IDomainService domainService = new DomainService(
            userService, taskService, projectService, adapterService
    );

    @NotNull
    private final IDataInterChangeService dataService = new DataInterChangeService(
            configService, domainService
    );

    @NotNull
    @Override
    public IUserService getUserService() {
        return this.userService;
    }

    @NotNull
    @Override
    public ISessionService getSessionService() {
        return this.sessionService;
    }

    @NotNull
    @Override
    public IAuthenticationService getAuthenticationService() {
        return this.authService;
    }

    @NotNull
    @Override
    public ITaskUserService getTaskService() {
        return this.taskService;
    }

    @NotNull
    @Override
    public IProjectUserService getProjectService() {
        return this.projectService;
    }

    @NotNull
    @Override
    public IDataInterChangeService getDataInterChangeService() {
        return this.dataService;
    }

    @NotNull
    @Override
    public IConfigurationService getConfigurationService() {
        return this.configService;
    }

    @NotNull
    @Override
    public IEntityManagerFactoryService getEntityManagerService() {
        return this.managerFactoryService;
    }

    @NotNull
    @Override
    public IAdapterLocatorService getAdapterService() {
        return this.adapterService;
    }

}