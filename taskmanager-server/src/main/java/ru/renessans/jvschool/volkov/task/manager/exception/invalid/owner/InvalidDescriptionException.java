package ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class InvalidDescriptionException extends AbstractException {

    @NotNull
    private static final String EMPTY_DESCRIPTION = "Ошибка! Параметр \"описание\" является пустым или null!\n";

    public InvalidDescriptionException() {
        super(EMPTY_DESCRIPTION);
    }

}