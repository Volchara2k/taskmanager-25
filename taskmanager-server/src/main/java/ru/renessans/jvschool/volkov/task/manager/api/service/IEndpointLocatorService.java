package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AbstractEndpoint;

import java.util.List;

@FunctionalInterface
public interface IEndpointLocatorService {

    @NotNull
    List<AbstractEndpoint> createEndpoints(@NotNull final IServiceLocatorService serviceLocator);

}