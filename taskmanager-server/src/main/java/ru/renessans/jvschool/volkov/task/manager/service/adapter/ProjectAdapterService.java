package ru.renessans.jvschool.volkov.task.manager.service.adapter;

import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.IProjectAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.model.Project;

import java.util.Objects;

public final class ProjectAdapterService implements IProjectAdapterService {

    @Nullable
    @Override
    public ProjectDTO toDTO(@Nullable final Project convertible) {
        if (Objects.isNull(convertible)) return null;
        return ProjectDTO.builder()
                .id(convertible.getId())
                .userId(convertible.getUserId())
                .title(convertible.getTitle())
                .description(convertible.getDescription())
                .creationDate(convertible.getCreationDate())
                .build();
    }

    @Nullable
    @Override
    public Project toModel(@Nullable final ProjectDTO convertible) {
        if (Objects.isNull(convertible)) return null;
        return Project.builder()
                .id(convertible.getId())
                .userId(convertible.getUserId())
                .title(convertible.getTitle())
                .description(convertible.getDescription())
                .creationDate(convertible.getCreationDate())
                .build();
    }

}