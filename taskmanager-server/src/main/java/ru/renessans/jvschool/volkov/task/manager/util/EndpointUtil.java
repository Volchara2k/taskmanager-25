package ru.renessans.jvschool.volkov.task.manager.util;

import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AbstractEndpoint;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.endpoint.InvalidEndpointException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.endpoint.InvalidEndpointsException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.endpoint.InvalidHostException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.endpoint.InvalidPortException;

import javax.xml.ws.Endpoint;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

@UtilityClass
public class EndpointUtil {

    @NotNull
    private static final Logger logger = Logger.getLogger(EndpointUtil.class.getName());

    @NotNull
    @SneakyThrows
    public Endpoint publish(
            @Nullable final AbstractEndpoint implementor,
            @Nullable final String host,
            @Nullable final Integer port
    ) {
        if (Objects.isNull(implementor)) throw new InvalidEndpointException();
        if (ValidRuleUtil.isNullOrEmpty(host)) throw new InvalidHostException();
        if (ValidRuleUtil.isNullOrEmpty(port)) throw new InvalidPortException();

        @NotNull final String title = implementor.getClass().getSimpleName();
        @NotNull final String address = String.format(
                "http://%s:%s/%s?WSDL", host, port, title
        );
        logger.log(Level.INFO, address);

        @NotNull final Endpoint release = Endpoint.publish(address, implementor);
        return release;
    }

    @NotNull
    @SneakyThrows
    public Collection<Endpoint> publish(
            @Nullable final Collection<AbstractEndpoint> implementors,
            @Nullable final String host,
            @Nullable final Integer port
    ) {
        if (ValidRuleUtil.isNullOrEmpty(implementors)) throw new InvalidEndpointsException();
        if (ValidRuleUtil.isNullOrEmpty(host)) throw new InvalidHostException();
        if (ValidRuleUtil.isNullOrEmpty(port)) throw new InvalidPortException();

        @NotNull final Collection<Endpoint> endpoints = new ArrayList<>();
        implementors.forEach(endpoint -> {
            @NotNull final Endpoint released = publish(endpoint, host, port);
            endpoints.add(released);
        });
        return endpoints;
    }

}