package ru.renessans.jvschool.volkov.task.manager.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IProjectUserRepository;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidTaskException;
import ru.renessans.jvschool.volkov.task.manager.model.Project;
import ru.renessans.jvschool.volkov.task.manager.model.User;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public final class ProjectUserRepository extends Repository<Project> implements IProjectUserRepository {

    @NotNull
    private final EntityManager entityManager;

    public ProjectUserRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
        this.entityManager = entityManager;
    }

    @NotNull
    @Override
    public Project persist(@NotNull final Project project) {
        @NotNull final User user = this.entityManager.find(User.class, project.getUserId());
        project.setUser(user);
        return super.persist(project);
    }

    @NotNull
    @SneakyThrows
    @Override
    public Project deleteByIndex(
            @NotNull final String userId,
            @NotNull final Integer index
    ) {
        @Nullable final Project project = getByIndex(userId, index);
        if (Objects.isNull(project)) throw new InvalidTaskException();
        this.entityManager.remove(project);
        return project;
    }

    @NotNull
    @SneakyThrows
    @Override
    public Project deleteById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        @Nullable final Project project = getById(userId, id);
        if (Objects.isNull(project)) throw new InvalidTaskException();
        this.entityManager.remove(project);
        return project;
    }

    @NotNull
    @SneakyThrows
    @Override
    public Project deleteByTitle(
            @NotNull final String userId,
            @NotNull final String title
    ) {
        @Nullable final Project project = getByTitle(userId, title);
        if (Objects.isNull(project)) throw new InvalidTaskException();
        this.entityManager.remove(project);
        return project;
    }

    @NotNull
    @Override
    public Collection<Project> deleteAll(
            @NotNull final String userId
    ) {
        @Nullable final Collection<Project> projects = getAll(userId);
        projects.forEach(this.entityManager::remove);
        return projects;
    }

    @NotNull
    @Override
    public Collection<Project> getAll(
            @NotNull final String userId
    ) {
        return this.entityManager.createQuery(
                "from Project WHERE user.id = :userId", Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public Project getByIndex(
            @NotNull final String userId,
            @NotNull final Integer index
    ) {
        @NotNull final List<Project> projects = this.entityManager.createQuery("SELECT e FROM Project e WHERE e.user.id = :userId", Project.class)
                .setParameter("userId", userId)
                .getResultList();
        if(projects.size() < index) return null;
        return projects.get(index);
    }

    @Nullable
    @Override
    public Project getById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        @NotNull final List<Project> projects =
                this.entityManager.createQuery("SELECT e FROM Project e WHERE e.user.id = :userId AND e.id = :id", Project.class)
                        .setParameter("userId", userId)
                        .setParameter("id", id)
                        .setMaxResults(1)
                        .getResultList();
        if (projects.isEmpty()) return null;
        return projects.get(0);
    }

    @Nullable
    @Override
    public Project getByTitle(
            @NotNull final String userId,
            @NotNull final String title
    ) {
        @NotNull final List<Project> projects =
                this.entityManager.createQuery("SELECT e FROM Project e WHERE e.user.id = :userId AND e.title = :title", Project.class)
                        .setParameter("userId", userId)
                        .setParameter("title", title)
                        .setMaxResults(1)
                        .getResultList();
        if (projects.isEmpty()) return null;
        return projects.get(0);
    }

    @NotNull
    @Override
    public Collection<Project> getAllRecords() {
        return this.entityManager.createQuery("SELECT e FROM Project e", Project.class)
                .getResultList();
    }

}