package ru.renessans.jvschool.volkov.task.manager.util;

import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.exception.illegal.IllegalHashAlgorithmException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.hash.InvalidHashLineException;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@UtilityClass
public final class HashUtil {

    @NotNull
    private static final String SEPARATOR_KEY = "y1Chy8w9ov31ck";

    private static final int ITERATOR_KEY = 15423;

    @NotNull
    @SneakyThrows
    public String getSaltHashLine(@Nullable final String line) {
        if (ValidRuleUtil.isNullOrEmpty(line)) throw new InvalidHashLineException();
        @NotNull String hashLine = line;
        for (int i = 0; i < ITERATOR_KEY; i++) {
            hashLine = getHashLine(SEPARATOR_KEY + line + SEPARATOR_KEY);
        }
        return hashLine;
    }

    @NotNull
    @SneakyThrows
    public String getHashLine(@Nullable final String line) {
        if (ValidRuleUtil.isNullOrEmpty(line)) throw new InvalidHashLineException();

        try {
            @NotNull final MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            final byte[] byteArray = messageDigest.digest(line.getBytes(StandardCharsets.UTF_8));
            @NotNull final StringBuilder stringBuilder = new StringBuilder();
            for (final byte aByte : byteArray) {
                stringBuilder.append(Integer.toHexString((aByte & 0xFF) | 0x100), 1, 3);
            }

            return stringBuilder.toString();
        } catch (@NotNull final NoSuchAlgorithmException exception) {
            throw new IllegalHashAlgorithmException(exception.getCause());
        }
    }

}