package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.api.service.*;

public interface IServiceLocatorRepository {

    @NotNull
    IUserService getUserService();

    @NotNull
    ISessionService getSessionService();

    @NotNull
    IAuthenticationService getAuthenticationService();

    @NotNull
    ITaskUserService getTaskService();

    @NotNull
    IProjectUserService getProjectService();

    @NotNull
    IDataInterChangeService getDataInterChangeService();

    @NotNull
    IConfigurationService getConfigurationService();

    @NotNull
    IEntityManagerFactoryService getEntityManagerService();

    @NotNull
    IAdapterLocatorService getAdapterService();

}