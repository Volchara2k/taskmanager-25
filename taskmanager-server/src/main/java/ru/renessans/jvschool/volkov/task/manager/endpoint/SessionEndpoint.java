package ru.renessans.jvschool.volkov.task.manager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.ISessionEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.IAdapterLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.service.ISessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.ISessionAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.enumeration.PermissionValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.SessionValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRole;
import ru.renessans.jvschool.volkov.task.manager.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

@WebService
public final class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    public SessionEndpoint() {
    }

    public SessionEndpoint(
            @NotNull final IServiceLocatorService serviceLocator
    ) {
        super(serviceLocator);
    }

    @WebMethod
    @WebResult(name = "openedSession", partName = "openedSession")
    @Nullable
    @Override
    public SessionDTO openSession(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session open = sessionService.openSession(login, password);
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();
        @Nullable final SessionDTO conversion = sessionAdapter.toDTO(open);
        return conversion;
    }

    @WebMethod
    @WebResult(name = "isClosed", partName = "isClosed")
    @Override
    public boolean closeSession(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();
        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @NotNull final Session current = sessionService.validateSession(conversion);
        return sessionService.closeSession(current);
    }

    @WebMethod
    @WebResult(name = "session", partName = "session")
    @Nullable
    @Override
    public SessionDTO validateSession(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();
        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @Nullable final Session validate = sessionService.validateSession(conversion);
        @Nullable final SessionDTO current = sessionAdapter.toDTO(validate);
        return current;
    }

    @WebMethod
    @WebResult(name = "session", partName = "session")
    @Nullable
    @Override
    public SessionDTO validateSessionWithCommandRole(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "commandRole", partName = "commandRole") @Nullable final UserRole command
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();
        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        @Nullable final Session current = sessionService.validateSession(conversion, command);
        @Nullable final SessionDTO validated = sessionAdapter.toDTO(current);
        return validated;
    }

    @WebMethod
    @WebResult(name = "sessionValidState", partName = "sessionValidState")
    @NotNull
    @Override
    public SessionValidState verifyValidSessionState(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();
        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        return sessionService.verifyValidSessionState(conversion);
    }

    @WebMethod
    @WebResult(name = "permissionValidState", partName = "permissionValidState")
    @NotNull
    @Override
    public PermissionValidState verifyValidPermissionState(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "commandRoles", partName = "commandRoles") @Nullable final UserRole commandRole
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final IAdapterLocatorService adapterService = super.serviceLocator.getAdapterService();
        @NotNull final ISessionAdapterService sessionAdapter = adapterService.getSessionAdapter();
        @Nullable final Session conversion = sessionAdapter.toModel(sessionDTO);
        return sessionService.verifyValidPermissionState(conversion, commandRole);
    }

}