package ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class InvalidProjectException extends AbstractException {

    @NotNull
    private static final String EMPTY_PROJECT = "Ошибка! Параметр \"проект\" является null!\n";

    public InvalidProjectException() {
        super(EMPTY_PROJECT);
    }

}