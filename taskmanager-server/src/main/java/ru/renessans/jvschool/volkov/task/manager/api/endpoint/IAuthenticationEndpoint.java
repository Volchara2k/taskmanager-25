package ru.renessans.jvschool.volkov.task.manager.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.dto.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.UserDTO;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRole;

public interface IAuthenticationEndpoint {

    @Nullable
    UserDTO signUpUser(
            @Nullable SessionDTO sessionDTO,
            @Nullable String login,
            @Nullable String password
    );

    @Nullable
    UserDTO signUpUserWithFirstName(
            @Nullable SessionDTO sessionDTO,
            @Nullable String login,
            @Nullable String password,
            @Nullable String firstName
    );

    @NotNull
    UserRole getUserRole(
            @Nullable SessionDTO sessionDTO
    );

}