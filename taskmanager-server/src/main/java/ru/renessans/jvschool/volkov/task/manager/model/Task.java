package ru.renessans.jvschool.volkov.task.manager.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@Table(name = "tm_task")
public final class Task extends AbstractUserOwner {

    @Nullable
    @ManyToOne
    private Project project;

    public Task(
            @NotNull final String title,
            @NotNull final String description
    ) {
        setTitle(title);
        setDescription(description);
    }

    public Task(
            @NotNull final String userId,
            @NotNull final String title,
            @NotNull final String description
    ) {
        setUserId(userId);
        setTitle(title);
        setDescription(description);
    }

    public Task(
            @NotNull final String id,
            @NotNull final String userId,
            @NotNull final String title,
            @NotNull final String description
    ) {
        setId(id);
        setUserId(userId);
        setTitle(title);
        setDescription(description);
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull final StringBuilder result = new StringBuilder();
        result.append("Заголовок задачи: ").append(this.getTitle());
        result.append(", описание задачи - ").append(this.getDescription());
        if (!Objects.isNull(this.project))
            result.append("\nДля проекта: ").append(this.project.getTitle());
        result.append("\nИдентификатор: ").append(super.getId()).append("\n");
        return result.toString();
    }

}