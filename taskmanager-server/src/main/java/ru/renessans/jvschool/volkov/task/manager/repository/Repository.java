package ru.renessans.jvschool.volkov.task.manager.repository;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.IRepository;
import ru.renessans.jvschool.volkov.task.manager.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

@RequiredArgsConstructor()
public class Repository<E extends AbstractModel> implements IRepository<E> {

    @NotNull
    private final EntityManager entityManager;

    @NotNull
    @Override
    public E persist(@NotNull final E value) {
        this.entityManager.persist(value);
        return value;
    }

    @NotNull
    @Override
    public E merge(@NotNull final E value) {
        this.entityManager.merge(value);
        return value;
    }

    @Override
    public boolean deleteAllRecords() {
        this.entityManager.clear();
        return true;
    }

    @Nullable
    @Override
    public E deleteRecord(@NotNull final E value) {
        this.entityManager.remove(value);
        return value;
    }

    @NotNull
    @Override
    public Collection<E> setAllRecords(@NotNull final Collection<E> values) {
        values.forEach(this::merge);
        return new ArrayList<>(values);
    }

    @Override
    public boolean deletedRecord(@NotNull final E value) {
        @Nullable final E deleted = deleteRecord(value);
        return !Objects.isNull(deleted);
    }

}