package ru.renessans.jvschool.volkov.task.manager.service;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointLocatorService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AbstractEndpoint;

import java.util.List;

public final class EndpointService implements IEndpointLocatorService {

    private final IEndpointLocatorRepository endpointRepository;

    public EndpointService(
            final IEndpointLocatorRepository endpointRepository
    ) {
        this.endpointRepository = endpointRepository;
    }

    @NotNull
    @Override
    public List<AbstractEndpoint> createEndpoints(@NotNull final IServiceLocatorService serviceLocator) {
        @NotNull final List<AbstractEndpoint> endpoints = this.endpointRepository.getAllEndpoints();
        endpoints.forEach(endpoint -> endpoint.setServiceLocator(serviceLocator));
        return endpoints;
    }

}