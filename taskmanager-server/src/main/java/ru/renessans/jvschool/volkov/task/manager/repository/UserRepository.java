package ru.renessans.jvschool.volkov.task.manager.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IUserRepository;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidUserException;
import ru.renessans.jvschool.volkov.task.manager.model.User;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public final class UserRepository extends Repository<User> implements IUserRepository {

    @NotNull
    private final EntityManager entityManager;

    public UserRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
        this.entityManager = entityManager;
    }

    @Nullable
    @Override
    public User getById(@NotNull final String id) {
        @NotNull final List<User> users = this.entityManager.createQuery("SELECT e FROM User e WHERE e.id = :id", User.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList();
        if (users.isEmpty()) return null;
        return users.get(0);
    }

    @Nullable
    @Override
    public User getByLogin(@NotNull final String login) {
        @NotNull final List<User> users = this.entityManager.createQuery("SELECT e FROM User e WHERE e.login = :login", User.class)
                .setParameter("login", login)
                .setMaxResults(1)
                .getResultList();
        if (users.isEmpty()) return null;
        return users.get(0);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User deleteById(@NotNull final String id) {
        @Nullable final User user = getById(id);
        if (Objects.isNull(user)) throw new InvalidUserException();
        this.entityManager.remove(user);
        return user;
    }

    @NotNull
    @SneakyThrows
    @Override
    public User deleteByLogin(@NotNull final String login) {
        @Nullable final User user = getByLogin(login);
        if (Objects.isNull(user)) throw new InvalidUserException();
        this.entityManager.remove(user);
        return user;
    }

    @NotNull
    @Override
    public Collection<User> getAllRecords() {
        return this.entityManager.createQuery("SELECT e FROM User e", User.class)
                .getResultList();
    }

}